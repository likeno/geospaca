"""Docker entrypoint for geospaca"""

import argparse
import functools
import os
import shlex
import sys
from pathlib import Path
from subprocess import run
import time

immediate_print = functools.partial(print, flush=True)


def wait_for_db(db_connection, max_tries=10, wait_seconds=3):
    current_try = 0
    while current_try < max_tries:
        try:
            db_connection.ensure_connection()
            immediate_print("Database is already up!")
            break
        except OperationalError:
            immediate_print("Database not responding yet ...")
            current_try += 1
            time.sleep(wait_seconds)
    else:
        raise RuntimeError(
            f"Database not responding after {max_tries} tries. "
            "Giving up"
        )


def create_environment():
    """Put any docker secrets in the environment"""
    secrets_directory = Path("/run/secrets")
    if secrets_directory.is_dir():
        for item in secrets_directory.iterdir():
            if item.is_file():
                contents = item.read_text().strip()
                os.environ[item.name] = contents


def migrate_db():
    external_process = run(
        shlex.split("django-admin migrate")
    )
    external_process.check_returncode()


def collect_static_files():
    external_process = run(
        shlex.split("django-admin collectstatic --no-input"),
    )
    external_process.check_returncode()


def create_django_superuser(username, email, password, django_user_model):
    """Check if a superuser already exists and create it if needed"""
    try:
        django_user_model.objects.get(username=username)
    except django_user_model.DoesNotExist:
        django_user_model.objects.create_superuser(username, email, password)


def complete_app_setup():
    management_commands = [
        # "loaddata api_initial_data.json",
    ]
    for command in management_commands:
        immediate_print(f"Running management command {command}...")
        external_process = run(
            shlex.split(f"django-admin {command}"),
        )
        external_process.check_returncode()


def run_server(
        num_workers: int = 2,
        port: int = 8000,
        debug: bool = False
):
    sys.stdout.flush()
    cmd = (
        f"gunicorn "
        f"--bind 0.0.0.0:{port} "
        f"--workers={num_workers} "
        f"--access-logfile=- "
        f"--error-logfile=- "
    )
    if debug:
        cmd += "--reload "
    cmd += "base.wsgi:application"
    command = shlex.split(cmd)
    os.execlp("gunicorn", *command)


def run_shell():
    cmd = shlex.split(f"django-admin shell")
    os.execlp("django-admin", *cmd)


def run_worker(num_threads: int = 1, debug: bool = False):
    cmd = shlex.split(
        f"django-admin rundramatiq "
        f"--threads {num_threads} "
        f"{'--reload' if debug else ''}"
    )
    os.execlp("django-admin", *cmd)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("cmd", choices=("runserver", "shell", "worker"))
    args, remaining = parser.parse_known_args()

    # The below initialization is a bit hacky and deserves an explanation:
    #
    # In order to make the container more robust, we want to make it do some
    # additional stuff before actually calling django:
    #
    # - Check that the database (container) is up and wait a bit for it
    # - Perform database migrations
    # - Check that a django superuser exists or create one
    # - Run additional management commands
    #
    # Some of these tasks are easier to do if we are able to use django
    # directly
    #
    # Check the official docs for more on how to initialize django in
    # standalone mode
    #
    #  https://docs.djangoproject.com/en/3.0/topics/settings/#calling-django-setup-is-required-for-standalone-django-usage

    # put any secrets into the current environment so that they may be picked
    # up by django
    create_environment()

    # add base dir to sys.path so that django can find it
    # app_dir = Path("~/geospaca").expanduser()
    # sys.path.append(str(app_dir))

    # now initialize django, as we'll need it later on to check DB and create
    # superuser
    import django
    django.setup()

    # finally we can import the django stuff that will be necessary
    from django.conf import settings
    from django.contrib.auth import get_user_model
    from django.db import connection
    from django.db.utils import OperationalError

    wait_for_db(connection)
    user_model = get_user_model()
    migrate_db()
    collect_static_files()
    create_django_superuser(
        username=os.environ.get("DJANGO_ADMIN_USERNAME", "admin"),
        email=os.environ.get("DJANGO_ADMIN_EMAIL", ""),
        password=os.environ["DJANGO_ADMIN_PASSWORD"],
        django_user_model=user_model
    )
    complete_app_setup()

    handler = {
        "runserver": functools.partial(
            run_server,
            num_workers=4,
            debug=settings.DEBUG
        ),
        "shell": run_shell,
        "worker": functools.partial(
            run_worker,
            num_threads=settings.NUM_DRAMATIQ_WORKER_THREADS,
            debug=settings.DEBUG
        ),
    }[args.cmd]
    handler(*remaining)
