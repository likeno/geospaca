# Call this script in order to get a geospaca development environment up
# Call it like this:
#
# bash docker/deploy.sh
#

REPO_ROOT=${REPO_ROOT:-$HOME/dev/geospaca}
DATA_ROOT=${DATA_ROOT:-$HOME/data/geospaca}

echo "REPO_ROOT=$REPO_ROOT"
echo "DATA_ROOT=$DATA_ROOT"

docker login registry.gitlab.com

export REPO_ROOT=$REPO_ROOT
export DATA_ROOT=$DATA_ROOT

docker stack deploy \
    --with-registry-auth \
    --prune \
    --compose-file docker/stack-dev.yml \
    geospaca
