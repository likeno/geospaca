import typing
import uuid

from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.postgres.fields import (
    ArrayField,
    JSONField
)
from polymorphic.models import PolymorphicModel

# FIXME: there is a circular import here
#from . import validators


class SpatialDataset(PolymorphicModel):

    class StorageType(models.TextChoices):
        FILESYSTEM = ("filesystem", "filesystem")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    bbox_native = models.PolygonField(srid=0)
    bbox_4326 = models.PolygonField(srid=4326)
    projection = models.CharField(max_length=200, default="4326")
    created = models.DateTimeField(auto_now_add=True)
    storage_type = models.CharField(
        max_length=50,
        choices=StorageType.choices,
        default=StorageType.FILESYSTEM
    )
    storage_configuration = JSONField(
        default=dict,
    )

    def __str__(self):
        return f"{self.name} ({self.id})"


class Coverage(SpatialDataset):
    last_refreshed = models.DateTimeField(null=True)


class Dimension(models.Model):

    class DimensionType(models.TextChoices):
        TEMPORAL = ("temporal", "temporal")
        NUMERICAL = ("numerical", "numerical")
        CATEGORICAL = ("categorical", "categorical")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    spatial_dataset = models.ForeignKey(
        "SpatialDataset",
        on_delete=models.CASCADE,
        related_name="dimensions"
    )
    type = models.CharField(max_length=20, choices=DimensionType.choices)
    units = models.CharField(max_length=50, blank=True)
    default = models.CharField(max_length=50, blank=True)
    multiple_values = models.BooleanField(default=False)
    nearest = models.BooleanField(default=False)
    current = models.BooleanField(default=False)
    extent = models.CharField(max_length=200, blank=True)
    resolution = models.CharField(max_length=200, blank=True)
    is_public = models.BooleanField(
        default=True,
        help_text="Whether this dimension should be made public or not"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    "name",
                    "spatial_dataset"
                ],
                name="unique_dimension_name_in_spatial_dataset"
            )
        ]


class Granule(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    coverage = models.ForeignKey(
        "Coverage",
        on_delete=models.CASCADE,
        related_name="granules"
    )
    dimension_values = JSONField()
    storage_details = JSONField()
    bbox_native = models.PolygonField(srid=0)
    bbox_4326 = models.PolygonField(srid=4326)


class Workspace(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, unique=True)
    ows_title = models.CharField(max_length=200, blank=True)
    enabled = models.BooleanField(default=True)
    default_projection = models.CharField(
        max_length=200,
        default="4326"
    )
    additional_projections = ArrayField(
        models.CharField(max_length=200),
        null=True,
        blank=True
    )
    bbox_native = models.PolygonField(srid=0)
    bbox_4326 = models.PolygonField(srid=4326)
    created = models.DateTimeField(auto_now_add=True)
    web_map_service_handler = models.CharField(
        max_length=255,
        blank=True,
        choices=[
            (c, c) for c in
            getattr(settings, "SERVICES", {}).get(
                "wms", {}).get("handlers", [])
        ],
        default=getattr(
            settings, "SERVICES", {}).get("wms", {}).get("handlers", [])[0]
    )


class Style(models.Model):

    class StyleType(models.TextChoices):
        MAPSERVER = ("mapserver", "mapserver")
        SLD = ("sld", "sld")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, unique=True)
    type = models.CharField(
        max_length=20,
        choices=StyleType.choices,
        default=StyleType.MAPSERVER
    )
    raw_value = models.TextField()


class Layer(models.Model):

    class LayerType(models.TextChoices):
        RASTER = ("raster", "raster")
        VECTOR_FIELD = ("vector_field", "vector_field")
        ISOLINE = ("isoline", "isoline")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, unique=True)
    enabled = models.BooleanField(default=True)
    advertised_projections = ArrayField(
        models.CharField(max_length=200, blank=True),
        null=True,
        blank=True
    )
    workspace = models.ForeignKey(
        "Workspace",
        on_delete=models.CASCADE,
        related_name="layers"
    )
    style = models.ForeignKey(
        "Style",
        on_delete=models.CASCADE,
        related_name="layers"
    )
    spatial_dataset = models.ForeignKey(
        "SpatialDataset",
        on_delete=models.CASCADE,
        related_name="layers"
    )
    type = models.CharField(
        max_length=50,
        choices=LayerType.choices,
        default=LayerType.RASTER
    )
    details = JSONField(null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    "name",
                    "workspace"
                ],
                name="unique_layer_name_in_workspace"
            )
        ]
