from rest_framework.schemas import get_schema_view
from django.views.generic import TemplateView
from django.urls import (
    include,
    path
)

from . import views

app_name = "geospaca"
urlpatterns = [
    path("", views.index, name="index"),
]
