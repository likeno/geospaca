import logging
import typing

from django.http import (
    Http404,
    HttpRequest,
    HttpResponse,
)
from django.shortcuts import (
    get_object_or_404,
)
from django.views import View

from . import (
    models,
    servicehandlers,
    utils,
)

logger = logging.getLogger(__name__)


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hi world!")


class OgcLegacyView(View):

    def get(self, request: HttpRequest, workspace_name: str):
        query_params = {k.lower(): v for k, v in request.GET.copy().items()}
        service_processor = {
            "wms": process_wms_request,
        }.get(query_params.get("service", "").lower())
        if service_processor is None:
            raise Http404("Invalid service")
        else:
            return service_processor(request, workspace_name, query_params)


class OgcLegacyWmsView(View):

    def get(self, request: HttpRequest, workspace_name: str):
        query_params = {k.lower(): v for k, v in request.GET.copy().items()}
        query_params.update(
            service="WMS",
        )
        return process_wms_request(request, workspace_name, query_params)


def process_wms_request(
        request: HttpRequest,
        workspace_name: str,
        query_parameters: typing.Dict
):
    workspace = get_object_or_404(models.Workspace, name=workspace_name)
    service_handler_path = workspace.web_map_service_handler
    service_handler_class = utils.lazy_import(service_handler_path)
    handler: servicehandlers.BaseHandler = service_handler_class()
    return handler.handle_request(workspace, query_parameters, request)
