from django.utils.text import slugify

from .. import models


def get_tileindex_db_view_name(spatial_dataset: models.SpatialDataset):
    return "_".join((
        "tileindex",
        slugify(str(spatial_dataset.id)).replace("-", "_"),
    ))
