"""Utilities for dealing with MapServer mapfiles"""

import collections.abc
import logging
import typing
from pathlib import Path

import mappyfile
from django.conf import settings
from django.contrib.sites.models import Site
from django.urls import reverse
from django.utils.text import slugify

from .. import models
from .utils import get_tileindex_db_view_name


BAND_REGEXP = "^[0-9]{1,5}$"
TEMPORAL_REGEXP = (
    r"^[0-9]{4}-[0-9]{2}-[0-9]{2}((T|\ )[0-9]{2}:[0-9]{2}:[0-9]{2})?$")
NUMERICAL_REGEXP = r"^[0-9]+(\.[0-9]+)?$"
CATEGORICAL_REGEXP = "^.*$"

logger = logging.getLogger(__name__)


def get_mapfile_path(workspace_name: str) -> Path:
    dirname = f"mapfiles{'_debug' if settings.DEBUG else ''}"
    return settings.DATA_ROOT / dirname / f"{workspace_name}.map"


def generate_mapfile(workspace: models.Workspace) -> typing.Dict:
    mapfile = get_mapfile_definition(workspace)
    layer_handlers = {
        models.Layer.LayerType.ISOLINE: (
            generate_tile_index_mapfile_layer,
            generate_mapfile_isoline_layer
        ),
        models.Layer.LayerType.RASTER: (
            generate_tile_index_mapfile_layer,
            generate_mapfile_raster_layer
        ),
        models.Layer.LayerType.VECTOR_FIELD: (
            generate_tile_index_for_vector_field_layer,
            generate_mapfile_vector_field_layer
        ),
    }
    for layer in workspace.layers.all():
        handlers = layer_handlers.get(layer.type)
        if handlers is not None:
            tileindex_handler, layer_handler = handlers
            tileindex_definition = tileindex_handler(layer)
            mapfile["layers"].append(tileindex_definition)
            layer_definition = layer_handler(
                layer, workspace, tileindex_definition["name"])
            mapfile["layers"].append(layer_definition)
        else:
            raise RuntimeError(f"Invalid layer type: {layer.type}")
    return mapfile


def save_mapfile(
        mapfile_definition: typing.Dict,
        workspace_name: str,
):
    mapfile_path = get_mapfile_path(workspace_name)
    mapfile_path.parent.mkdir(parents=True, exist_ok=True)
    errors = mappyfile.validate(mapfile_definition)
    if len(errors) == 0:
        with mapfile_path.open("w", encoding="utf-8") as fh:
            fh.write(mappyfile.dumps(mapfile_definition))
        logger.info(f"mapfile_path: {mapfile_path!r}")
    else:
        raise RuntimeError(f"invalid mapfile: {errors}")
    return mapfile_path


def get_online_resource_url(workspace: models.Workspace):
    protocol = "https" if settings.USE_SSL else "http"
    domain = Site.objects.get_current().domain
    relative_path = reverse(
        "ogc", kwargs={"workspace_name": workspace.name})
    return f"{protocol}://{domain}{relative_path}?"


def get_mapfile_definition(workspace: models.Workspace) -> typing.Dict:
    additional_projections = workspace.additional_projections or []
    advertised_projections = (
            [workspace.default_projection] + additional_projections)
    mapfile = {
        "__type__": "map",
    }
    if settings.DEBUG:
        mapfile["debug"] = 5
        mapfile["config"] = {
            "ms_errorfile": "/tmp/mapserver_errors.txt",
            "cpl_debug": "on",
            "proj_debug": "on",
        }
    if settings.MAPSERVER_FONTSET_PATH is not None:
        mapfile["fontset"] = str(settings.MAPSERVER_FONTSET_PATH)
    mapfile.update({
        "name": workspace.name,
        "projection": [
            f"init=epsg:{workspace.default_projection}",
        ],
        "web": {
            "__type__": "web",
            "metadata": {
                "__type__": "metadata",
                "ows_title": workspace.ows_title,
                "ows_onlineresource": get_online_resource_url(workspace),
                "ows_enable_request": "*",
                "wms_feature_info_mime_type": "application/json",
                "ows_srs": " ".join(
                    [f"EPSG:{proj}" for proj in advertised_projections])
            }
        },
        "extent": list(workspace.bbox_native.extent),
        "shapepath": str(settings.DATA_ROOT),
        "imagecolor": [0, 0, 0],
        "imagetype": "png",
        "size": [800, 600],
        "status": "on",
        "outputformats": [
            {
                "__type__": "outputformat",
                "name": "png",
                "mimetype": "image/png",
                "driver": "agg/png",
                "extension": "png",
                "imagemode": "rgb",
                "transparent": "false",
            },
        ],
        "legend": {
            "__type__": "legend",
            "keysize": [20, 10],
            "keyspacing": [5, 5],
            "label": {
                "__type__": "label",
                "size": "medium",
                "offset": [0, 0],
                "shadowsize": [1, 1],
                "type": "bitmap",
            },
            "status": "off",
        },
        "layers": []
    })
    symbols_dir = settings.MAPSERVER_SYMBOLS_DIR
    logger.debug(f"symbols_dir: {symbols_dir}")
    if symbols_dir is not None:
        raw_symbol_definitions = get_symbol_definitions(symbols_dir)
        logger.debug(f"raw_symbol_definitions: {raw_symbol_definitions}")
        loaded_symbols = mappyfile.loads(raw_symbol_definitions)
        logger.debug(f"loaded_symbols: {loaded_symbols}")
        if len(loaded_symbols) > 0:
            mapfile["symbols"] = loaded_symbols
    return mapfile


def get_symbol_definitions(symbols_dir: Path, encoding="utf-8") -> str:
    result = ""
    for path in symbols_dir.iterdir():
        if path.is_file():
            result = "\n".join((result, path.read_text(encoding=encoding)))
        elif path.is_dir():
            result = "\n".join((result, get_symbol_definitions(path)))
    return result


def get_layer_advertised_projections(
        layer: models.Layer,
        workspace: models.Workspace
) -> typing.List:
    item_projection = layer.spatial_dataset.projection
    additional = layer.advertised_projections or []
    workspace_default = workspace.default_projection
    workspace_additional = workspace.additional_projections or []
    unique = set(
        [item_projection]
    ).union(
        set(additional)
    ).union(
        [workspace_default]
    ).union(
        set(workspace_additional)
    )
    return unique


def _generate_mapfile_layer_skeleton(
        layer: models.Layer,
        workspace: models.Workspace,
        tileindex_name: typing.Optional[str] = None,
        declare_dimensions: typing.Optional[typing.List] = None
) -> typing.Dict:

    extent = layer.spatial_dataset.bbox_native.extent
    advertised_projections = get_layer_advertised_projections(layer, workspace)
    projections = " ".join(
        f"EPSG:{srs.upper()}" for srs in advertised_projections)
    layer_definition = {
        "__type__": "layer",
        "debug": 5 if settings.DEBUG else 0,
        "name": layer.name,
        "validation": {},
        "projection": [
            f"init=epsg:{layer.spatial_dataset.projection.lower()}"
        ],
        "extent": list(extent),
        "metadata": {
            "__type__": "metadata",
            "ows_title": layer.name,
            "ows_srs": projections,
            "ows_include_items": "all",
            "ows_extent": " ".join(str(i) for i in extent),
            "gml_include_items": "all",
            "wms_dimensionlist": ""
        },
        "status": "off",
        "template": "templates/blank.json",
        "units": "meters",  # FIXME
    }
    band_name = get_layer_band_name(layer.spatial_dataset)
    layer_definition["validation"] = {
        f"default_{band_name}": "1",
        band_name: BAND_REGEXP,
    }
    if layer.style.type == models.Style.StyleType.MAPSERVER.value:
        loaded = mappyfile.loads(layer.style.raw_value)
        if isinstance(loaded, collections.abc.Sequence):
            layer_definition.update(classes=loaded)
        else:
            layer_definition.update(classes=[loaded])
    if tileindex_name is not None:
        layer_definition.update(
            tileindex=tileindex_name,
            tileitem="location"
        )
    else:
        layer_definition["data"] = "%data%"

    public_dimensions = layer.spatial_dataset.dimensions.filter(is_public=True)
    if declare_dimensions is None:
        dims_to_advertise = public_dimensions
    else:
        dims_to_advertise = [d for d in public_dimensions if d.name in declare_dimensions]
    layer_definition["metadata"]["wms_dimensionlist"] = ", ".join(
        [d.name for d in dims_to_advertise])
    for dimension in dims_to_advertise:
        config_prefix = f"wms_{dimension.name}"
        if dimension.name != "time":
            # for some reason MapServer expects the metadata for the TIME
            # dimension to be named like `wms_timeitem` while for all other
            # dimensions, it expects something like `wms_elevation_item`
            config_prefix += "_"
        layer_definition["metadata"].update({
            f"{config_prefix}item": dimension.name,
            f"{config_prefix}units": dimension.units,
            f"{config_prefix}extent": dimension.extent,
            f"{config_prefix}default": dimension.default,
        })
    return layer_definition


def get_layer_band_name(spatial_dataset: models.SpatialDataset):
    slugified_id = slugify(str(spatial_dataset.id))
    return f"band_{slugified_id}"


def get_vector_field_layer_band_names(spatial_dataset: models.SpatialDataset):
    base_name = get_layer_band_name(spatial_dataset)
    return (
        f"{base_name}_u",
        f"{base_name}_v",
    )


def generate_mapfile_raster_layer(
        layer: models.Layer,
        workspace: models.Workspace,
        tileindex_name: typing.Optional[str] = None,
) -> typing.Dict:
    layer_definition = _generate_mapfile_layer_skeleton(
        layer, workspace, tileindex_name)
    band_name = get_layer_band_name(layer.spatial_dataset)
    layer_definition.update({
        "type": "raster",
        "processing": [f"BANDS=%{band_name}%"],
    })
    return layer_definition


def generate_mapfile_vector_field_layer(
        layer: models.Layer,
        workspace: models.Workspace,
        tileindex_name: typing.Optional[str] = None,
) -> typing.Dict:
    has_time_dim = layer.spatial_dataset.dimensions.filter(
        name="time").exists()
    declare_dims = ["time"] if has_time_dim else None
    layer_definition = _generate_mapfile_layer_skeleton(
        layer, workspace, tileindex_name, declare_dimensions=declare_dims)
    u_band_name, v_band_name = get_vector_field_layer_band_names(
        layer.spatial_dataset)
    layer_definition.update({
        "validation": {
            f"default_{u_band_name}": "1",
            u_band_name: BAND_REGEXP,
            f"default_{v_band_name}": "2",
            v_band_name: BAND_REGEXP,
        },
        "type": "point",
        "connectiontype": "uvraster",
        "processing": [
            f"BANDS=%{u_band_name}%,%{v_band_name}%",
            f"UV_SPACING={layer.details['spacing']}",
            f"UV_SIZE_SCALE={layer.details['size_scale']}"
        ]
    })
    return layer_definition


# TODO: add support for smoothing preprocessing
# TODO: add support for smoothing size
def generate_mapfile_isoline_layer(
        layer: models.Layer,
        workspace: models.Workspace,
        tileindex_name: typing.Optional[str] = None,
) -> typing.Dict:
    layer_definition = _generate_mapfile_layer_skeleton(
        layer, workspace, tileindex_name)
    band_name = get_layer_band_name(layer.spatial_dataset)
    processing_info = [
        f"BANDS=%{band_name}%",
        "LABEL_NO_CLIP=ON",
        "CONTOUR_ITEM=elevation"
    ]
    interval = layer.details.get("interval")
    fixed_levels = layer.details.get("fixed_levels")
    if interval is not None:
        processing_info.append(f"CONTOUR_INTERVAL={interval}")
    elif fixed_levels is not None:
        processing_info.append(f"CONTOUR_LEVELS={fixed_levels}")
    layer_definition.update({
        "type": "line",
        "connectiontype": "contour",
        "processing": processing_info,
    })
    generalization_algorithm = layer.details.get("generalization_algorithm")
    if generalization_algorithm is not None:
        layer_definition["geomtransform"] = _get_isoline_geomtransform(
            generalization_algorithm,
            layer.details.get("generalization_tolerance"),
            smoothing_iteration=layer.details.get("smoothing_iterations")
        )
    return layer_definition


def _get_isoline_geomtransform(
        generalization_algorithm: str,
        generalization_tolerance: float,
        smoothing_size: typing.Optional[int] = 3,
        smoothing_iteration: typing.Optional[int] = 1,
        smoothing_preprocessing: typing.Optional[str] = None,
):
    generalization_call = (
        f"{generalization_algorithm}("
        f"[shape], "
        f"{generalization_tolerance}*[data_cellsize]"
        f")"
    )
    smoothing_params = [
        generalization_call,
        str(smoothing_size),
        str(smoothing_iteration)
    ]
    if smoothing_preprocessing is not None:
        smoothing_params.append(f"{smoothing_preprocessing!r}")
    smoothing_call = f"(smoothsia({', '.join(smoothing_params)}))"
    return smoothing_call


def complete_tile_index_layer_skeleton(
        layer: models.Layer,
        validation: typing.Dict,
        processing: typing.List[str]
) -> typing.Dict:
    db_view_name = get_tileindex_db_view_name(layer.spatial_dataset)
    return {
        "__type__": "layer",
        "name": f"index_for_{layer.name}",
        "debug": 5 if settings.DEBUG else 0,
        "type": "polygon",
        "connectiontype": "POSTGIS",
        "connection": get_postgis_connection_string(),
        "data": [
            (
                f"geom FROM {db_view_name} "
                f"using unique id "
                f"using srid={layer.spatial_dataset.projection}"
            )
        ],
        "metadata": {
            "__type__": "metadata",
            # this hides tileindex layer from GetCapabilities responses
            "ows_enable_request": "!GetCapabilities"
        },
        "processing": processing,
        "validation": validation
    }


def generate_tile_index_for_vector_field_layer(
        layer: models.Layer
) -> typing.Dict:
    """
    Generate a tileindex layer definition suitable for vector field layers.
    """

    # geospaca implements vector field layers as Mapserver uvraster layers with
    # a tileindex. Mapserver currently has some limitations regarding the usage
    # of arbitrary dimensions on uvraster layers that have a tileindex. As such
    # we use a temporary workaround:
    # - the tileindex layer's VALIDATION treats all dimensions except TIME as
    #   normal Mapserver variables that have runtime susbtitution applied to
    #   them
    # - we explicitly generate a FILTER (or more properly a
    #   PROCESSING=NATIVE_FILTER) combining all dimensions except TIME in
    #   order to compensate for mapserver's lack of automatically generating
    #   such filter
    dimensions = layer.spatial_dataset.dimensions.all()
    validation_dim_prefixes = {
        d.name: True if d.name != "time" else False for d in dimensions}
    validation = build_tileindex_layer_validation(
        dimensions,
        use_dim_name_prefix=validation_dim_prefixes
    )
    native_filter = generate_native_filter_expression(dimensions)
    processing_instructions = [
        "CLOSE_CONNECTION=DEFER",
        f"NATIVE_FILTER={native_filter}"
    ]
    return complete_tile_index_layer_skeleton(
        layer, validation, processing_instructions)


def generate_native_filter_expression(
        dimensions: typing.Iterable[models.Dimension]) -> str:
    parts = []
    for dim in dimensions:
        if dim.name == "time":
            # mapserver UVRASTER has explicit support for TIME dimension when
            # using a tileindex - it lacks support for other dimensions though
            continue
        parts.append(f"{dim.name}='%dim_{dim.name}%'")
    return " AND ".join(parts)


def generate_tile_index_mapfile_layer(layer: models.Layer) -> typing.Dict:
    """Generate a mapfile LAYER to serve as a TILEINDEX for a raster layer.

    For specifying a raster layer in a mapserver mapfile geospaca uses two
    LAYER definitions - one of them is the actual layer and the other is used
    by the first as a TILEINDEX. This allows complex filtering to be used in
    order to perform dimensional subsetting of the layer (be it spatial,
    temporal or other).

    """

    validation = build_tileindex_layer_validation(
        layer.spatial_dataset.dimensions.all())
    processing_instructions = [
        "CLOSE_CONNECTION=DEFER",
    ]
    return complete_tile_index_layer_skeleton(
        layer, validation, processing_instructions)


def build_tileindex_layer_validation(
        dimensions: typing.Iterable[models.Dimension],
        use_dim_name_prefix: typing.Optional[typing.Dict[str, bool]] = None
) -> typing.Dict:
    use_prefix = dict(use_dim_name_prefix) if use_dim_name_prefix else {}
    result = {}
    for dim in dimensions:
        regexp = {
            models.Dimension.DimensionType.TEMPORAL: TEMPORAL_REGEXP,
            models.Dimension.DimensionType.NUMERICAL: NUMERICAL_REGEXP,
            models.Dimension.DimensionType.CATEGORICAL: CATEGORICAL_REGEXP,
        }[dim.type]
        prefix = f"dim_" if use_prefix.get(dim.name, False) else ""
        name = f"{prefix}{dim.name}"
        result.update({
            f"default_{name}": dim.default,
            name: regexp
        })
    return result


def get_postgis_connection_string():
    db_info = settings.DATABASES["default"]
    params = {
        "user": db_info["USER"],
        "password": db_info["PASSWORD"],
        "host": db_info["HOST"],
        "port": db_info["PORT"],
        "dbname": db_info["NAME"],
    }
    return " ".join(f"{k}={v}" for k, v in params.items() if v != "")
