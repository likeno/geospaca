import logging
import shlex
import subprocess
import typing
from http.client import parse_headers
from io import BytesIO

from django.conf import settings

logger = logging.getLogger(__name__)


def call_mapserv(
        query_params: typing.Dict
) -> typing.Tuple[typing.Mapping[str, str], bytes, str]:
    query_string = _serialize_mapserv_payload(**query_params)
    logger.debug(f"query_string: {query_string}")
    completed_mapserv = subprocess.run(
        shlex.split(f"{settings.MAPSERV_PATH} QUERY_STRING='{query_string}'"),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    raw_headers, body = completed_mapserv.stdout.partition(b"\r\n\r\n")[::2]
    headers = parse_headers(BytesIO(raw_headers))
    return headers, body, str(completed_mapserv.stderr, "utf-8")


def _serialize_mapserv_payload(**payload):
    return "&".join([f"{k}={str(v)}" for k, v in payload.items()])

