import logging
import typing
import uuid

from django.db import (
    connection,
    transaction
)

from .. import models
from .utils import get_tileindex_db_view_name

logger = logging.getLogger(__name__)


def regenerate_coverage_tileindex_view(coverage_id: str) -> str:
    """"(Re)generate the DB view that is used by mapserver as a tileindex.

    This function updates the DB view that is used as a tileindex in
    mapserver mapfiles. The update process entails the following steps:

    * Remove a possibly pre-existing view;
    * Create a new view;
    * Populate the view with data;

    """

    coverage = models.Coverage.objects.get(pk=uuid.UUID(coverage_id))
    db_view_name = get_tileindex_db_view_name(coverage)
    logger.info(f"db_view_name: {db_view_name}")
    with connection.cursor() as db_cursor:
        with transaction.atomic():
            drop_coverage_tileindex_view(db_view_name, db_cursor)
            create_coverage_tileindex_view(db_view_name, coverage, db_cursor)
            refresh_coverage_tileindex_view(db_view_name, db_cursor)
        db_cursor.execute(f"VACUUM ANALYZE {db_view_name}")
    return db_view_name


@transaction.atomic
def create_coverage_tileindex_view(
        view_name: str,
        coverage: models.Coverage,
        cursor
):
    dimensions = coverage.dimensions.filter(is_public=True)
    view_generation_ddl = _generate_create_view_query(
        view_name, coverage, public_dimensions=dimensions)
    logger.info(f"ddl: {view_generation_ddl}")
    cursor.execute(view_generation_ddl)
    cursor.execute(
        f"CREATE UNIQUE INDEX ON {view_name} (id)"
    )
    gist_index_name = f"{view_name}_geom_gist_idx"
    cursor.execute(
        f"CREATE INDEX {gist_index_name} ON {view_name} "
        f"USING GIST (geom gist_geometry_ops_nd)"
    )
    # create a multi-column index for the dimensions
    dim_index_name = f"{view_name}_{'_'.join(d.name for d in dimensions)}_idx"
    cursor.execute(
        f"CREATE INDEX {dim_index_name} ON {view_name} ("
        f"{', '.join(d.name for d in dimensions)})"
    )


def drop_coverage_tileindex_view(view_name, cursor):
    return cursor.execute(f"DROP MATERIALIZED VIEW IF EXISTS {view_name}")


def refresh_coverage_tileindex_view(
        view_name: str,
        cursor,
        concurrently: typing.Optional[bool] = False
):
    return cursor.execute(
        f"REFRESH MATERIALIZED VIEW"
        f" {'CONCURRENTLY' if concurrently else ''} {view_name}"
    )


def _generate_create_view_query(
        name: str,
        coverage: models.Coverage,
        public_dimensions: typing.Iterable
) -> str:
    """Generate SQL DDL commands for creating a view for a coverage's tileindex

    This function outputs the textual SQL that can be sent to the DB in order
    to request the creation of a materialized view.

    NOTES:

    - The materialized view does not cast temporal dimensions other than
      `time` to `timestamp` because mapserver expects them to be text and
      cannot deal with them properly unless they are text

    - Note that the generated SQL includes the `WITH NO DATA` fragment, which
      means that when it is executed, the corresponding materialized view is
      created but it will not be immediately populated.

    """
    location_column_ddl = _prepare_tileindex_location_column(coverage)
    dimension_columns_ddl = _prepare_tileindex_dimension_values_columns(
        public_dimensions)
    dimensions_ddl = ", ".join([f"{val} AS {alias}" for val, alias in dimension_columns_ddl])
    query = f"""
    CREATE MATERIALIZED VIEW {name} AS (
        SELECT
            id,
            {location_column_ddl} AS location,
            ST_SetSRID(bbox_native, {coverage.projection})::geometry(POLYGON, {coverage.projection}) AS geom,
            {dimensions_ddl}
        FROM geospaca_granule
        WHERE coverage_id = '{coverage.id}'
    )
    WITH NO DATA
    """
    return query


def _prepare_tileindex_dimension_values_columns(
        public_dimensions: typing.Iterable
) -> typing.List[typing.Tuple[str, str]]:
    columns = []
    # Mapserver expects all additional dimensions (i.e. not `time` or
    # `elevation`) to have a data type of text - This means we cannot
    # cast them to timestamp or float
    for dim in public_dimensions:
        if dim.name == "time":
            column_ddl = (
                f"to_timestamp(dimension_values->>'{dim.name}', "
                f"'YYYY-MM-DD HH24:MI:SS')"
            )
        elif dim.name == "elevation":
            column_ddl = f"CAST(dimension_values->>'{dim.name}' AS float)"
        else:
            column_ddl = f"dimension_values->>'{dim.name}'"
        columns.append((column_ddl, dim.name))
    return columns


def _prepare_tileindex_location_column(spatial_dataset: models.SpatialDataset):
    # depending on the coverage's storage_type, build the appropriate column
    if spatial_dataset.storage_type == models.SpatialDataset.StorageType.FILESYSTEM:
        result = _prepare_filesystem_location_column(spatial_dataset)
    else:
        raise NotImplementedError
    return result


def _prepare_filesystem_location_column(
        spatial_dataset: models.SpatialDataset,
):
    storage_config = spatial_dataset.storage_configuration
    result_fragments = []
    for structure_item in storage_config["structure"]:
        dimension = spatial_dataset.dimensions.get(
            name=structure_item["dimension"])
        if dimension.type == models.Dimension.DimensionType.TEMPORAL:
            path_fragment = _prepare_column_fragment_temporal_dim(
                dimension.name, structure_item["temporal_fragments"])
            result_fragments.append(path_fragment)
        elif dimension.type == models.Dimension.DimensionType.NUMERICAL:
            raise NotImplementedError
        elif dimension.type == models.Dimension.DimensionType.CATEGORICAL:
            raise NotImplementedError
        else:
            raise NotImplementedError
    return _prepare_column_ddl(
        storage_config["root_data_dir"],
        filename=f"storage_details->>'filename'",
        path_fragments=result_fragments
    )


def _prepare_column_ddl(
        base_dir: str,
        filename: str,
        path_fragments: typing.Optional[typing.List[str]] = None
):
    file_path_ddl = f"CONCAT_WS('/', '{base_dir}'"
    if len(path_fragments or []) > 0:
        joined_fragments = ", ".join(f"{frag}" for frag in path_fragments)
        file_path_ddl = ", ".join((file_path_ddl, joined_fragments))
    file_path_ddl += f", {filename})"
    print(f"file_path_ddl: {file_path_ddl}")
    parts = [
        "storage_details->>'subdataset_prefix'",
        file_path_ddl,
        "storage_details->>'subdataset_suffix'",
    ]
    joined_parts = ", ".join(parts)
    return f"CONCAT_WS(':', {joined_parts})"


def _prepare_column_fragment_temporal_dim(
        dimension: str,
        temporal_fragments: typing.List[str]):
    items = []
    for fragment in temporal_fragments:
        timestamp_item = (
            f"to_timestamp("
            f"dimension_values->>'{dimension}', "
            f"'YYYY-MM-DD HH24:MI:SS'"
            f")"
        )
        part_fragment = f"EXTRACT({fragment.upper()} FROM {timestamp_item})"
        if fragment in ("month", "day", "hour", "minute", "second"):
            part_fragment = f"lpad({part_fragment}::text, 2, '0')"
        items.append(part_fragment)
    joined_items = ", ".join(items) if len(items) > 0 else "''"
    return f"CONCAT_WS('/', {joined_items})"
