import logging
import typing

from django.http import QueryDict
from django_filters import rest_framework as filters
from rest_framework_json_api.django_filters import DjangoFilterBackend

from . import models

logger = logging.getLogger(__name__)


class MyDjangoFilterBackend(DjangoFilterBackend):
    """A custom filter backend in order to allow filtering on JSONField

    Geospaca stores dimension values in JSONField.

    It is not possible to hardcode the name of a dimension, they are
    configurable by the end user. As such, this

    """

    def get_filterset_kwargs(self, request, queryset, view):
        dimension_values_prefix = "dimension_values"
        kwargs = super().get_filterset_kwargs(request, queryset, view)
        dimension_names = []
        new_data = QueryDict(mutable=True)
        for k, v in kwargs["data"].items():
            if k.startswith(dimension_values_prefix):
                dimension_name = k.rpartition("__")[-1]
                if new_data.get("dimension_values"):
                    new_data["dimension_values"] = "***".join((new_data["dimension_values"], v))
                else:
                    new_data["dimension_values"] = v
                dimension_names.append(dimension_name)
            else:
                new_data[k] = v
        new_filter_keys = []
        for filter_key in kwargs["filter_keys"]:
            if filter_key.startswith(dimension_values_prefix):
                new_filter_keys.append(dimension_values_prefix)
            else:
                new_filter_keys.append(filter_key)
        new_kwargs = kwargs.copy()
        new_kwargs.update(
            data=new_data,
            filter_keys=new_filter_keys,
            dimension_names=dimension_names,
        )
        logger.debug(f"kwargs: {kwargs}")
        logger.debug(f"new_kwargs: {new_kwargs}")
        return new_kwargs


class GranuleFilterSet(filters.FilterSet):

    dimension_values = filters.CharFilter(
        field_name="dimension_values",
        method="filter_dimension_values"
    )

    def __init__(
            self,
            *args,
            dimension_names: typing.Optional[typing.List[str]] = None,
            **kwargs
    ):
        self.dimension_names = dimension_names or []
        logger.debug(f"init locals: {locals()}")
        super().__init__(*args, **kwargs)

    class Meta:
        model = models.Granule
        fields = [
            "dimension_values",
        ]

    def filter_dimension_values(self, queryset, name, value):

        logger.debug(f"locals: {locals()}")
        qs_kwargs = {}
        for index, dim_value in enumerate(value.split("***")):
            logger.debug(f"index: {index}")
            logger.debug(f"dim_value: {dim_value}")
            dim_lookup = "__".join((name, self.dimension_names[index], "exact"))
            qs_kwargs[dim_lookup] = dim_value
            logger.debug(f"dim_lookup: {dim_lookup}")
        logger.debug(f"qs_kwargs: {qs_kwargs}")
        return queryset.filter(**qs_kwargs)

