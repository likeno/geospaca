"""Dramatiq tasks"""

import functools
import logging

import dramatiq
from django.db import (
    connection,
    connections,
)

from . import models
from .mapserver import (
    mapfilehandler,
    tileindexhandler,
)
from .mapserver.utils import get_tileindex_db_view_name

logger = logging.getLogger(__name__)


def sanitize_db_connection(func):

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        logger.debug("sanitizing django database connection...")
        conn = connections["default"]
        conn.close_if_unusable_or_obsolete()
        return func(*args, **kwargs)

    return wrapper


@dramatiq.actor
def greet_world(greeting: str = "hello"):
    print(f"{greeting} world!")


@dramatiq.actor
@sanitize_db_connection
def generate_mapfile(
        workspace_id: str,
):
    workspace = models.Workspace.objects.get(pk=workspace_id)
    mapfile_definition = mapfilehandler.generate_mapfile(workspace)
    logger.debug(f"mapfile_definition: {mapfile_definition}")
    mapfile_path = mapfilehandler.save_mapfile(
        mapfile_definition, workspace.name)
    logger.info(f"saved mapfile in {mapfile_path}")
    return mapfile_path


@dramatiq.actor
@sanitize_db_connection
def remove_mapfile(
        workspace_id: str,
):
    workspace = models.Workspace.objects.get(pk=workspace_id)
    mapfile_path = mapfilehandler.get_mapfile_path(workspace.name)
    try:
        mapfile_path.unlink()
        result = mapfile_path
    except FileNotFoundError:
        logger.debug(f"mapfile {mapfile_path} does not exist. Ignoring...")
        result = None
    return result


@dramatiq.actor
@sanitize_db_connection
def generate_coverage_tileindex_database_view(coverage_id: str):
    view_name = tileindexhandler.regenerate_coverage_tileindex_view(
        coverage_id)
    return view_name


@dramatiq.actor
@sanitize_db_connection
def delete_coverage_tileindex_database_view(coverage_id: str):
    coverage = models.Coverage.objects.get(pk=coverage_id)
    view_name = get_tileindex_db_view_name(coverage)
    with connection.cursor() as cursor:
        tileindexhandler.drop_coverage_tileindex_view(view_name, cursor)


@dramatiq.actor
@sanitize_db_connection
def refresh_coverage_tileindex_database_view(coverage_id: str):
    coverage = models.Coverage.objects.get(pk=coverage_id)
    view_name = get_tileindex_db_view_name(coverage)
    with connection.cursor() as cursor:
        tileindexhandler.refresh_coverage_tileindex_view(
            view_name, cursor, concurrently=True)
    return view_name
