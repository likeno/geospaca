from django.apps import AppConfig


class GeospacaConfig(AppConfig):
    name = 'geospaca'
