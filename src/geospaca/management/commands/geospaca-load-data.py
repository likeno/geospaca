"""Management commands for loading several geospaca items

This management command makes use of the geospaca API serializers to perform
validation of the input data in a similar way as the one done when using the
API via HTTP requests.

"""

import datetime as dt
import typing
from pathlib import Path

import yaml
from django.core.management.base import (
    BaseCommand,
    CommandParser,
)

from geospaca import models
from geospaca.api import serializers


class Command(BaseCommand):
    help = "Load default data"

    def add_arguments(self, parser: CommandParser):
        parser.add_argument(
            "path",
            help="YAML files with data to be loaded",
            nargs="+",
            type=Path
        )

    def handle(self, *args, **options):
        items = {
            "dimensions": [],
            "coverages": [],
            "granules": [],
            "styles": [],
            "workspaces": [],
            "layers": []
        }
        for path in options["path"]:
            path: Path
            self.stdout.write(f"Processing {path!r}...")
            with path.open(encoding="utf-8") as fh:
                data = yaml.safe_load(fh)
                self.stdout.write(
                    "Handling coverages, dimensions and granules...")
                for coverage_definition in data.get("coverages", []):
                    coverage, dimensions, granules = get_coverage(
                        coverage_definition["name"],
                        coverage_definition["projection"],
                        coverage_definition["bbox_native"],
                        coverage_definition["storage_type"],
                        coverage_definition["storage_configuration"],
                        dimension_infos=coverage_definition.get(
                            "dimensions", []),
                        granule_infos=coverage_definition.get("granules", []),
                        stdout=self.stdout.write,
                    )
                    items["coverages"].append(coverage)
                    items["dimensions"].extend(dimensions)
                    items["granules"].extend(granules)

                self.stdout.write("Handling styles...")
                for style_definition in data.get("styles", []):
                    style = get_style(
                        style_definition["name"],
                        style_type=style_definition["style_type"],
                        raw_value=style_definition["raw_value"],
                        stdout=self.stdout.write,
                    )
                    items["styles"].append(style)

                self.stdout.write("Handling workspaces and layers...")
                coverages = items["coverages"] or models.Coverage.objects.all()
                styles = items["styles"] or models.Style.objects.all()
                for workspace_definition in data.get("workspaces", []):
                    workspace, layers = get_workspace(
                        workspace_definition["name"],
                        workspace_definition["default_projection"],
                        workspace_definition["bbox"],
                        generated_coverages=coverages,
                        generated_styles=styles,
                        layer_definitions=workspace_definition["layers"],
                        stdout=self.stdout.write,
                    )
                    items["workspaces"].append(workspace)
                    items["layers"].extend(layers)
        self.stdout.write(f"Referenced items: {items}")


def get_coverage(
        name: str,
        projection: str,
        bbox_native: str,
        storage_type: str,
        storage_configuration: typing.Dict,
        dimension_infos: typing.Optional[typing.List[typing.Dict]] = None,
        granule_infos: typing.Optional[typing.List[typing.Dict]] = None,
        stdout: typing.Callable = print,
) -> typing.Tuple[
    models.Coverage,
    typing.List[models.Dimension],
    typing.List[models.Granule]
]:
    data = {
        "name": name,
        "projection": projection,
        "bbox_native": bbox_native,
        "storage_type": storage_type,
        "storage_configuration": storage_configuration
    }
    try:
        existing = models.Coverage.objects.get(name=name)
    except models.Coverage.DoesNotExist:
        stdout(f"Creating coverage {data}...")
        serializer = serializers.CoverageSerializer(data=data)
    else:
        stdout(f"Updating coverage {existing}...")
        serializer = serializers.CoverageSerializer(existing, data=data)
    serializer.is_valid(raise_exception=True)
    coverage = serializer.save()
    dimensions = []
    for dimension_info in dimension_infos:
        dimension = get_dimension(
            coverage,
            dimension_info["name"],
            dimension_info["type"],
            dimension_info["extent"],
            dimension_info["default"],
            dimension_info.get("is_public", True),
            stdout=stdout
        )
        dimensions.append(dimension)
    granules = []
    for granule_info in granule_infos:
        granule = get_granule(
            coverage,
            granule_info["bbox_native"],
            granule_info["storage_details"],
            granule_info["dimension_values"],
            stdout=stdout
        )
        granules.append(granule)
    refresh_time = dt.datetime.now() + dt.timedelta(hours=1)
    update_serializer = serializers.CoverageSerializer(
        instance=coverage,
        data={
            "last_refreshed": refresh_time.isoformat(),
        },
        partial=True
    )
    update_serializer.is_valid(raise_exception=True)
    updated_coverage = update_serializer.save()
    return updated_coverage, dimensions, granules


def get_dimension(
        coverage: models.Coverage,
        name: str,
        dimension_type: str,
        extent: str,
        default: str,
        is_public: bool,
        stdout: typing.Optional[typing.Callable] = print
) -> models.Dimension:
    data = {
        "name": name,
        "type": dimension_type,
        "extent": extent,
        "default": default,
        "is_public": is_public,
        "spatial_dataset": {
            "type": "SpatialDataset",
            "id": str(coverage.id)
        }
    }
    try:
        existing = models.Dimension.objects.get(
            name=name,
            spatial_dataset=coverage
        )
    except models.Dimension.DoesNotExist:
        stdout(f"Creating dimension {data}...")
        serializer = serializers.DimensionSerializer(data=data)
    else:
        stdout(f"Updating dimension {existing}...")
        serializer = serializers.DimensionSerializer(existing, data=data)
    serializer.is_valid(raise_exception=True)
    return serializer.save()


def get_granule(
        coverage: models.Coverage,
        bbox_native: str,
        storage_details: typing.Dict,
        dimension_values: typing.Dict,
        stdout: typing.Optional[typing.Callable] = print
):
    encoded_dimension_values = {
        k: str(v) if isinstance(v, dt.datetime) else v
        for k, v in dimension_values.items()
    }
    stdout(f"encoded_dimension_values: {encoded_dimension_values}")
    data = {
        "bbox_native": bbox_native,
        "storage_details": storage_details,
        "coverage": {
            "type": "Coverage",
            "id": str(coverage.id),
        },
        "dimension_values": encoded_dimension_values,
    }
    try:
        existing = models.Granule.objects.get(
            coverage=coverage,
            dimension_values=encoded_dimension_values
        )
    except models.Granule.DoesNotExist:
        stdout(f"Creating granule {data}...")
        serializer = serializers.GranuleSerializer(data=data)
    else:
        serializer = serializers.GranuleSerializer(existing, data=data)
    serializer.is_valid(raise_exception=True)
    return serializer.save()


def get_style(
        name: str,
        style_type: str,
        raw_value: str,
        stdout: typing.Callable = print
) -> models.Style:
    data = {
        "name": name,
        "type": style_type,
        "raw_value": raw_value
    }
    try:
        existing = models.Style.objects.get(name=name)
    except models.Style.DoesNotExist:
        stdout(f"Creating style {data}...")
        serializer = serializers.StyleSerializer(data=data)
    else:
        serializer = serializers.StyleSerializer(existing, data=data)
    serializer.is_valid(raise_exception=True)
    return serializer.save()


def get_workspace(
        name: str,
        default_projection: str,
        bbox_native: str,
        generated_coverages: typing.Optional[typing.List] = None,
        generated_styles: typing.Optional[typing.List] = None,
        layer_definitions: typing.Optional[typing.List[typing.Dict]] = None,
        stdout: typing.Callable = print
) -> typing.Tuple[models.Workspace, typing.List[models.Layer]]:
    data = {
        "name": name,
        "default_projection": default_projection,
        "bbox_native": bbox_native,
    }
    try:
        existing = models.Workspace.objects.get(name=name)
    except models.Workspace.DoesNotExist:
        stdout(f"Creating workspace {data}...")
        serializer = serializers.WorkspaceSerializer(data=data)
    else:
        serializer = serializers.WorkspaceSerializer(existing, data=data)
    serializer.is_valid(raise_exception=True)
    workspace = serializer.save()
    generated_layers = []
    coverages = generated_coverages or models.Coverage.objects.all()
    styles = generated_styles or models.Style.objects.all()
    for layer_definition in layer_definitions or []:
        layer = get_layer(
            layer_definition["name"],
            layer_definition["layer_type"],
            layer_definition["coverage"],
            layer_definition["style"],
            workspace,
            generated_spatial_datasets=coverages,
            generated_styles=styles,
            details=layer_definition.get("details"),
            stdout=stdout,
        )
        generated_layers.append(layer)
    return workspace, generated_layers


def get_layer(
        name: str,
        layer_type: str,
        spatial_dataset_index: int,
        style_index: int,
        workspace: models.Workspace,
        generated_spatial_datasets: typing.List,
        generated_styles: typing.List,
        details: typing.Optional[typing.Dict] = None,
        stdout: typing.Callable = print
) -> models.Layer:
    spatial_dataset = generated_spatial_datasets[spatial_dataset_index]
    style = generated_styles[style_index]
    data = {
        "name": name,
        "type": layer_type,
        "details": details,
        "workspace": {
            "type": "Workspace",
            "id": str(workspace.id)
        },
        "spatial_dataset": {
            "type": "SpatialDataset",
            "id": str(spatial_dataset.id)
        },
        "style": {
            "type": "Style",
            "id": str(style.id)
        },
    }
    try:
        existing = models.Layer.objects.get(
            name=name,
            spatial_dataset=spatial_dataset
        )
    except models.Layer.DoesNotExist:
        stdout(f"Creating layer {data}...")
        serializer = serializers.LayerSerializer(data=data)
    else:
        serializer = serializers.LayerSerializer(existing, data=data)
    serializer.is_valid(raise_exception=True)
    return serializer.save()
