"""Management commands for setting up the default site's domain."""

from django.contrib.sites.models import Site
from django.core.management.base import (
    BaseCommand,
    CommandError,
    CommandParser,
)



class Command(BaseCommand):
    help = "Setup the domain for geospaca"

    def add_arguments(self, parser: CommandParser):
        parser.add_argument(
            "domain",
            help="Domain of the website (e.g. example.com)",
        )
        parser.add_argument(
            "--site-id",
            default=1,
            type=int,
            help=(
                "ID of the site for which the given domain name applies. "
                "Defaults to %(default)s"
            ),
        )

    def handle(self, *args, **options):
        domain_name = options["domain"]
        site_id = options["site_id"]
        try:
            site = Site.objects.get(id=site_id)
        except Site.DoesNotExist:
            raise CommandError(f"Invalid site id: {site_id}")
        site.domain = domain_name
        site.name = domain_name
        site.save()
        self.stdout.write("Done!")
