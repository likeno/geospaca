import itertools
import time
import typing

import requests


def check_health_http_resource(url: str) -> bool:
    response = requests.get(url)
    return response.status_code == 200


def wait_for_service(
        url: str,
        retry_seconds: typing.Optional[int] = 3,
        num_tries: typing.Optional[int] = 5,
        handler: typing.Optional[typing.Callable] = check_health_http_resource,
        stdout: typing.Callable = print,
) -> bool:
    stdout(f"Checking if service is healthy...")
    result = False
    for current_try in itertools.count():
        healthy = handler(url)
        if healthy:
            stdout(f"Service is healthy")
            result = True
            break
        elif current_try < num_tries:
            time.sleep(retry_seconds)
        else:
            stdout(
                f"Service still not healthy after {num_tries} tries - "
                f"Giving up"
            )
            break
    return result
