"""Management command for generating a django template project"""

import shutil
from pathlib import Path

from django.core.management.base import (
    BaseCommand,
    CommandParser,
)
from django.conf import settings


def parse_destination_dir(value: str) -> Path:
    return Path(value).expanduser()


class Command(BaseCommand):
    help = (
        "Generate a django project template based on the current geospaca "
        "codebase"
    )

    def handle(self, *args, **options):
        repo_root = settings.BASE_DIR.parent
        destination_dir = repo_root.parent / "geospaca-template"
        self.stdout.write(f"destination-dir: {destination_dir}")
        shutil.rmtree(str(destination_dir), ignore_errors=True)
        destination_dir.mkdir(parents=True, exist_ok=True)
        to_modify = (
            "pyproject.toml",
        )
        for item in collect_items(repo_root):
            item_destination = Path(
                str(item).replace(str(repo_root), str(destination_dir))
            )
            self.stdout.write(f"item: {item} destination: {item_destination}")
            if item.is_dir():
                self.stdout.write(f"Creating directory {item_destination}...")
                item_destination.mkdir(parents=True, exist_ok=True)
            elif item.name in to_modify:
                self.stdout.write(f"Modifying {item_destination}...")
                contents = item.read_text()
                modified_contents = modify_contents(contents)
                item_destination.write_text(modified_contents)
            else:
                self.stdout.write(f"Copying {item_destination}...")
                shutil.copy(str(item), str(item_destination))


def collect_items(root: Path):
    discard_suffixes = [
        ".pyc",
    ]
    discard_dir_names = [
        Path(settings.STATIC_ROOT).name,
        ".git",
        "__pycache__",
    ]
    for path in root.iterdir():
        if path.is_dir():
            if path.name not in discard_dir_names:
                yield path
                yield from collect_items(path)
        else:
            if path.suffix not in discard_suffixes:
                yield path


def modify_contents(contents: str):
    return contents.replace("geospaca", "{{ project_name }}")
