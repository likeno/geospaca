"""Management commands for creating new geospaca items

This management command makes use of the geospaca API serializers to perform
validation of the input data in a similar way as the one done when using the
API via HTTP requests.

"""

import typing

from django.core.management.base import (
    BaseCommand,
    CommandParser,
)

from geospaca.api import serializers


class Command(BaseCommand):
    help = "Add new geospaca items"

    def add_arguments(self, parser: CommandParser):
        subparsers = parser.add_subparsers(title="Add new geospaca items")
        parser_create_workspace = subparsers.add_parser("workspace")
        parser_create_workspace.add_argument("name")
        parser_create_workspace.add_argument("default_projection")
        parser_create_workspace.add_argument("bbox")
        parser_create_workspace.set_defaults(func=create_workspace)

    def handle(self, *args, **options):
        opts = dict(options)
        handler = opts.pop("func")
        result = handler(**opts)


def create_workspace(
        name: str,
        default_projection: str,
        bbox: str,
        **kwargs
):
    serializer = serializers.WorkspaceSerializer(data={
        "name": name,
        "default_projection": default_projection,
        "bbox": bbox
    })
    serializer.is_valid(raise_exception=True)
    return serializer.save()


def create_layer():
    raise NotImplementedError


def create_response_handler():
    raise NotImplementedError


def create_dimension():
    raise NotImplementedError


def create_coverage(
        name: str,
        bbox: str,
        projection: str,
        storage_type: str
):
    serializer = serializers.CoverageSerializer(data={
        "name": name,
        "bbox": bbox,
        "projection": projection,
        "storage_type": storage_type,
    })
    serializer.is_valid(raise_exception=True)
    return serializer.save()


def create_granule():
    raise NotImplementedError
