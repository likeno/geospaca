# Generated by Django 3.0.3 on 2020-02-28 13:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('geospaca', '0017_remove_spatialdataset_response_handlers'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ResponseHandler',
        ),
    ]
