from django.urls import (
    include,
    path
)
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r"workspaces", views.WorkspaceViewSet)
router.register(r"layers", views.LayerViewSet)
router.register(r"styles", views.StyleViewSet)
router.register(r"dimensions", views.DimensionViewSet)
router.register(r"coverages", views.CoverageViewSet)
router.register(r"granules", views.GranuleViewSet)
router.register(r"spatial_datasets", views.SpatialDatasetViewSet)

app_name = "api"
urlpatterns = [
    path("", include(router.urls)),
    path(
        "workspaces/<uuid:pk>/<str:related_field>/",
        views.WorkspaceViewSet.as_view({"get": "retrieve_related"}),
        name="workspace-related"
    ),
    path(
        "workspaces/<uuid:pk>/relationships/<str:related_field>/",
        views.WorkspaceRelationshipView.as_view(),
        name="workspace-relationships"
    ),
    path(
        "styles/<uuid:pk>/<str:related_field>/",
        views.StyleViewSet.as_view({"get": "retrieve_related"}),
        name="style-related"
    ),
    path(
        "styles/<uuid:pk>/relationships/<str:related_field>/",
        views.StyleRelationshipView.as_view(),
        name="style-relationships"
    ),
    path(
        "granules/<uuid:pk>/<str:related_field>/",
        views.GranuleViewSet.as_view({"get": "retrieve_related"}),
        name="granule-related"
    ),
    path(
        "granules/<uuid:pk>/relationships/<str:related_field>/",
        views.GranuleRelationshipView.as_view(),
        name="granule-relationships"
    ),
    path(
        "coverages/<uuid:pk>/<str:related_field>/",
        views.CoverageViewSet.as_view({"get": "retrieve_related"}),
        name="coverage-related"
    ),
    path(
        "coverages/<uuid:pk>/relationships/<str:related_field>/",
        views.CoverageRelationshipView.as_view(),
        name="coverage-relationships"
    ),
    path(
        "dimensions/<uuid:pk>/<str:related_field>/",
        views.DimensionViewSet.as_view({"get": "retrieve_related"}),
        name="dimension-related"
    ),
    path(
        "dimensions/<uuid:pk>/relationships/<str:related_field>/",
        views.DimensionRelationshipView.as_view(),
        name="dimension-relationships"
    ),
    path(
        "layers/<uuid:pk>/<str:related_field>/",
        views.LayerViewSet.as_view({"get": "retrieve_related"}),
        name="layer-related"
    ),
    path(
        "layers/<uuid:pk>/relationships/<str:related_field>/",
        views.LayerRelationshipView.as_view(),
        name="layer-relationships"
    ),
]
