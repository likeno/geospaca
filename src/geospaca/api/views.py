import logging

from django.db.models import QuerySet, Manager
from rest_framework.exceptions import NotFound
from rest_framework.fields import get_attribute
from rest_framework.generics import get_object_or_404
from rest_framework.relations import PKOnlyObject
from rest_framework.response import Response
from rest_framework.serializers import SkipField
from rest_framework_json_api.views import (
    ModelViewSet,
    RelationshipView
)

from .. import (
    filters,
    models,
    tasks,
)

from . import serializers

logger = logging.getLogger(__name__)


class RelatedPaginatedModelViewSet(ModelViewSet):
    """Adds pagination and filtering to related endpoints"""

    def get_related_viewset(self):
        raise NotImplementedError

    def retrieve_related(self, request, *args, **kwargs):
        instance = self.get_related_instance()
        if isinstance(instance, Manager):
            qs = self.filter_related(instance)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                result = self.get_paginated_response(serializer.data)
            else:
                serializer = self.get_serializer(qs, many=True)
                result = Response(serializer.data)
        elif callable(instance):
            instance = instance()
            serializer = self.get_serializer(instance)
            result = Response(serializer.data)
        elif instance is None:
            result = Response(data=None)
        else:
            serializer = self.get_serializer(instance)
            result = Response(serializer.data)
        return result

    def filter_related(self, queryset: QuerySet):
        """
        Given a queryset, filter it with whichever filter backend and
        filter classes are in use by the related viewset.
        """

        related_viewset = self.get_related_viewset()
        for backend in list(related_viewset.filter_backends):
            queryset = backend().filter_queryset(
                self.request, queryset, related_viewset)
        return queryset

    def get_related_parent_object(self):
        """
        Very similar to the standard `get_object()` but does not apply any
        filtering. A related endpoint's filtering parameters (if any) are
        to be applied to the related items, not the parent.

        """

        queryset = self.get_queryset()
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
        )
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj

    def get_related_instance(self):
        parent_obj = self.get_related_parent_object()
        parent_serializer = self.serializer_class(parent_obj)
        field_name = self.get_related_field_name()
        field = parent_serializer.fields.get(field_name, None)
        if field is not None:
            try:
                instance = field.get_attribute(parent_obj)
            except SkipField:
                instance = get_attribute(parent_obj, field.source_attrs)
            else:
                if isinstance(instance, PKOnlyObject):
                    # need whole object
                    instance = get_attribute(parent_obj, field.source_attrs)
            return instance
        else:
            try:
                return getattr(parent_obj, field_name)
            except AttributeError:
                raise NotFound


class WorkspaceViewSet(ModelViewSet):
    queryset = models.Workspace.objects.all()
    serializer_class = serializers.WorkspaceSerializer

    def perform_destroy(self, instance: models.Workspace):
        tasks.remove_mapfile.send(str(instance.id))
        super().perform_destroy(instance)


class WorkspaceRelationshipView(RelationshipView):
    queryset = models.Workspace.objects


class LayerViewSet(ModelViewSet):
    queryset = models.Layer.objects.all()
    serializer_class = serializers.LayerSerializer
    filterset_fields = {
        "name": ("exact", "iexact", "contains", "icontains",),
    }


class LayerRelationshipView(RelationshipView):
    queryset = models.Layer.objects


class StyleViewSet(ModelViewSet):
    queryset = models.Style.objects.all()
    serializer_class = serializers.StyleSerializer


class StyleRelationshipView(RelationshipView):
    queryset = models.Style.objects


class DimensionViewSet(ModelViewSet):
    queryset = models.Dimension.objects.all()
    serializer_class = serializers.DimensionSerializer

    def perform_destroy(self, instance: models.Dimension):
        spatial_dataset = instance.spatial_dataset
        super().perform_destroy(instance)
        if isinstance(spatial_dataset, models.Coverage):
            tasks.generate_coverage_tileindex_database_view(
                str(spatial_dataset.id))


class DimensionRelationshipView(RelationshipView):
    queryset = models.Dimension.objects


class CoverageViewSet(RelatedPaginatedModelViewSet):
    queryset = models.Coverage.objects.all()
    serializer_class = serializers.CoverageSerializer
    filterset_fields = {
        "name": ("exact", "iexact", "contains", "icontains",),
    }

    def get_related_viewset(self):
        return {
            "granules": GranuleViewSet,
        }.get(self.get_related_field_name(), self)

    def perform_destroy(self, instance: models.Coverage):
        super().perform_destroy(instance)
        tasks.delete_coverage_tileindex_database_view.send(str(instance.id))


class CoverageRelationshipView(RelationshipView):
    queryset = models.Coverage.objects


class GranuleViewSet(ModelViewSet):
    queryset = models.Granule.objects.all()
    serializer_class = serializers.GranuleSerializer
    filterset_class = filters.GranuleFilterSet
    filter_backends = [
        filters.MyDjangoFilterBackend,
    ]


class GranuleRelationshipView(RelationshipView):
    queryset = models.Granule.objects


class SpatialDatasetViewSet(ModelViewSet):
    queryset = models.SpatialDataset.objects.all()
    serializer_class = serializers.SpatialDatasetSerializer


class SpatialDatasetRelationshipView(RelationshipView):
    queryset = models.SpatialDataset.objects
