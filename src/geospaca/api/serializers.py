import datetime as dt
import logging
import typing

import pyproj
from django.core.exceptions import ObjectDoesNotExist
from pydantic import ValidationError
from rest_framework_json_api import serializers
from rest_framework_json_api.relations import HyperlinkedRelatedField
from shapely import (
    geometry,
    wkt,
)
from shapely.ops import transform

from .. import (
    models,
    tasks,
    utils,
    validators,
)

logger = logging.getLogger(__name__)


def reproject_geometry(
        geom: geometry.Polygon,
        source_epsg: int,
        destination_epsg: typing.Optional[int] = 4326
):
    coordinate_reprojector = pyproj.Transformer.from_proj(
        pyproj.Proj(source_epsg),
        pyproj.Proj(destination_epsg)
    )
    return transform(coordinate_reprojector.transform, geom)


class MyHyperlinkedRelatedField(HyperlinkedRelatedField):
    """A HyperlinkedRelatedField that knows how to deserialize related data"""

    default_error_messages = {
        "invalid_id": "Could not find resource with the provided id",
    }

    def to_internal_value(self, data):
        try:
            instance = self.get_queryset().get(pk=data["id"])
        except ObjectDoesNotExist:
            self.fail("invalid_id")
        # logger.debug(f"locals: {locals()}")
        return instance
        #return super().to_internal_value(data)


class CoverageSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Coverage
        fields = [
            "id",
            "name",
            "bbox_native",
            "bbox_4326",
            "projection",
            "created",
            "storage_type",
            "storage_configuration",
            "last_refreshed",
            "dimensions",
            "layers",
            "granules",
            "url",
        ]
        extra_kwargs = {
            "url": {"view_name": "api:coverage-detail"},
            "bbox_4326": {"read_only": True},
            "projection": {"default": "4326"},
        }

    layers = HyperlinkedRelatedField(
        required=False,
        queryset=models.Layer.objects,
        many=True,
        related_link_view_name="api:coverage-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:coverage-relationships"
    )

    granules = HyperlinkedRelatedField(
        required=False,
        queryset=models.Granule.objects,
        many=True,
        related_link_view_name="api:coverage-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:coverage-relationships"
    )

    dimensions = HyperlinkedRelatedField(
        required=False,
        queryset=models.Dimension.objects,
        many=True,
        related_link_view_name="api:coverage-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:coverage-relationships"
    )

    related_serializers = {
        "dimensions": "geospaca.api.serializers.DimensionSerializer",
        "layers": "geospaca.api.serializers.LayerSerializer",
        "granules": "geospaca.api.serializers.GranuleSerializer",
    }

    def validate_last_refreshed(self, value: dt.datetime):
        now = dt.datetime.now(tz=dt.timezone.utc)
        if value < now:
            raise serializers.ValidationError(
                "last_refreshed value cannot be in the past")
        return now.strftime("%Y-%m-%d %H:%M:%S")

    def validate(self, data: typing.Dict):
        """Validate fields that require access to object-level data

        - Make sure the provided ``storage_configuration`` is consistent with
          the ``storage_type``
        - Validate `bbox_native` according to the supplied `default_projection`
        - Populate the `bbox_4326` field by converting the coordinates
          supplied in the `bbox_native` field

        """

        self.instance: models.Coverage
        provided_storage_type = data.get("storage_type")
        if self.instance:
            existing_storage_type = self.instance.storage_type
            existing_configuration = self.instance.storage_configuration
            is_same_type = provided_storage_type == existing_storage_type
            if provided_storage_type and not is_same_type:
                # about to change to a new storage_type
                existing_configuration = {}
        else:
            default_storage_type = (
                self.Meta.model._meta.get_field("storage_type").default.value)
            existing_storage_type = default_storage_type
            existing_configuration = {}
        storage_type = provided_storage_type or existing_storage_type
        type_validator_class = {
            models.SpatialDataset.StorageType.FILESYSTEM.value: (
                validators.FilesystemStorageConfiguration)
        }[storage_type]
        provided_configuration = existing_configuration.copy()
        provided_configuration.update(data.get("storage_configuration", {}))
        try:
            validator = type_validator_class(**provided_configuration)
        except ValidationError as exc:
            raise serializers.ValidationError(exc.json())
        data.update(
            storage_type=storage_type,
            storage_configuration=validator.dict(),
        )
        logger.debug(
            f"data[storage_configuration]: {data['storage_configuration']}")
        bbox_native = data.get("bbox_native")
        if bbox_native is not None:
            bbox_native_geom = wkt.loads(bbox_native)
            bbox_4326_geom = reproject_geometry(
                bbox_native_geom,
                int(data["projection"])
            )
            data["bbox_4326"] = wkt.dumps(bbox_4326_geom)
        return data

    def save(self, **kwargs):
        coverage = super().save(**kwargs)
        tasks.generate_coverage_tileindex_database_view.send(str(coverage.id))
        return coverage

    def update(self, instance: models.Coverage, validated_data: typing.Dict):
        updated_instance = super().update(instance, validated_data)
        if validated_data.get("last_refreshed") is not None:
            logger.info(
                "tileindex database view will be refreshed by the worker "
                "process..."
            )
            tasks.refresh_coverage_tileindex_database_view.send(
                str(updated_instance.id))
        return updated_instance


class LayerSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Layer
        fields = [
            "id",
            "name",
            "enabled",
            "advertised_projections",
            "workspace",
            "style",
            "spatial_dataset",
            "type",
            "details",
            "url",
        ]
        extra_kwargs = {
            "url": {"view_name": "api:layer-detail"}
        }

    workspace = MyHyperlinkedRelatedField(
        queryset=models.Workspace.objects,
        related_link_view_name="api:layer-related",
        self_link_view_name="api:layer-relationships",
    )

    style = MyHyperlinkedRelatedField(
        queryset=models.Style.objects,
        related_link_view_name="api:layer-related",
        self_link_view_name="api:layer-relationships",
    )

    spatial_dataset = MyHyperlinkedRelatedField(
        queryset=models.SpatialDataset.objects,
        related_link_view_name="api:layer-related",
        self_link_view_name="api:layer-relationships",
    )

    related_serializers = {
        "workspace": "geospaca.api.serializers.WorkspaceSerializer",
        "style": "geospaca.api.serializers.StyleSerializer",
        "spatial_dataset": "geospaca.api.serializers.SpatialDatasetSerializer",
    }

    def validate(self, data: typing.Dict):
        """Validate fields that require access to object-level data

        - Check that `details` are consistent with the declared `type`

        """

        if self.instance:
            existing_type = self.instance.type
            existing_details = self.instance.details or {}
            provided_type = data.get("type", existing_type)
            provided_details = data.get("details") or {}
            existing_details.update(provided_details)
        else:
            provided_type = data.get(
                "type",
                models.Layer._meta.get_field("type").default.value
            )
            existing_details = data.get("details") or {}

        details_validator_class = {
            models.Layer.LayerType.RASTER: validators.RasterLayerDetails,
            models.Layer.LayerType.VECTOR_FIELD: (
                validators.VectorFieldLayerDetails),
            models.Layer.LayerType.ISOLINE: validators.IsolineLayerDetails,
        }[provided_type]


        try:
            details_validator = details_validator_class(**existing_details)
        except ValidationError as exc:
            raise serializers.ValidationError(exc.json())
        data.update(
            details=details_validator.dict(),
        )
        logger.debug(
            f"data[details]: {data['details']}")
        return data


class WorkspaceSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Workspace
        fields = [
            "id",
            "name",
            "ows_title",
            "enabled",
            "default_projection",
            "additional_projections",
            "bbox_native",
            "bbox_4326",
            "created",
            "layers",
            "url",
        ]
        extra_kwargs = {
            "url": {"view_name": "api:workspace-detail"},
            "bbox_4326": {"read_only": True},
            "default_projection": {"default": "4326"}
        }

    layers = HyperlinkedRelatedField(
        required=False,
        queryset=models.Layer.objects,
        many=True,
        related_link_view_name="api:workspace-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:workspace-relationships"
    )

    related_serializers = {
        "layers": LayerSerializer,
    }

    def validate(self, data: typing.Dict):
        """Validate fields that require access to object-level data

        - Validate `bbox_native` according to the supplied `default_projection`
        - Populate the `bbox_4326` field by converting the coordinates
          supplied in the `bbox_native` field

        """

        bbox_native = data["bbox_native"]
        bbox_native_geom = wkt.loads(bbox_native)
        bbox_4326_geom = reproject_geometry(
            bbox_native_geom,
            int(data["default_projection"])
        )
        data["bbox_4326"] = wkt.dumps(bbox_4326_geom)
        return data

    def save(self, **kwargs):
        workspace = super().save(**kwargs)
        tasks.generate_mapfile.send(str(workspace.id))
        return workspace


class StyleSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Style
        fields = [
            "name",
            "type",
            "raw_value",
            "layers",
            "url",
        ]
        extra_kwargs = {
            "url": {"view_name": "api:style-detail"}
        }

    layers = HyperlinkedRelatedField(
        required=False,
        queryset=models.Layer.objects,
        many=True,
        related_link_view_name="api:style-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:style-relationships"
    )

    related_serializers = {
        "layers": LayerSerializer,
    }


class DimensionSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Dimension
        fields = [
            "id",
            "name",
            "type",
            "units",
            "default",
            "multiple_values",
            "nearest",
            "current",
            "extent",
            "resolution",
            "spatial_dataset",
            "is_public",
            "url",
        ]
        extra_kwargs = {
            "url": {"view_name": "api:dimension-detail"}
        }

    spatial_dataset = MyHyperlinkedRelatedField(
        queryset=models.SpatialDataset.objects,
        related_link_view_name="api:dimension-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:dimension-relationships"
    )

    related_serializers = {
        "spatial_dataset": "geospaca.api.serializers.SpatialDatasetSerializer",
    }

    def save(self, **kwargs):
        """Save the dimension and update other internal artifacts

        In addition to saving the dimension record in the DB, this method will:

        - Update any relevant workspace's mapfiles
        - Update the relevant tileindex

        """

        dimension = super().save(**kwargs)
        # This dimension is related to some spatial_dataset which in turn
        # might be related to existing layers. As such, we need to update the
        # mapfiles for the workspace(s) where these layers belong
        workspace_ids = set()
        for layer in dimension.spatial_dataset.layers.all():
            workspace_ids.add(str(layer.workspace_id))
        for id_ in workspace_ids:
            tasks.generate_mapfile.send(id_)
        if isinstance(dimension.spatial_dataset, models.Coverage):
            tasks.generate_coverage_tileindex_database_view.send(
                str(dimension.spatial_dataset.id))
        return dimension


class GranuleSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Granule
        fields = [
            "id",
            "coverage",
            "bbox_native",
            "dimension_values",
            "storage_details",
            "url",
        ]
        extra_kwargs = {
            "url": {"view_name": "api:granule-detail"}
        }

    coverage = MyHyperlinkedRelatedField(
        queryset=models.Coverage.objects,
        related_link_view_name="api:granule-related",
        related_link_url_kwarg="pk",
        self_link_view_name="api:granule-relationships"
    )

    related_serializers = {
        "coverage": CoverageSerializer
    }

    def validate(self, data: typing.Dict):
        """Validate fields that require access to object-level data

        - Make sure the provided ``dimension_values`` are consistent with the
          granule's coverage's ``dimensions``

        - Make sure the provided ``storage_details`` are consistent with the
          granule's coverage's ``storage_type``

        - Validate `bbox_native` according to the supplied `default_projection`

        - Populate the `bbox_4326` field by converting the coordinates
          supplied in the `bbox_native` field

        """

        provided_storage_details = data.get("storage_details")
        if provided_storage_details is not None:
            if self.instance:
                existing_storage_details = self.instance.storage_details or {}
                existing_storage_details.update(provided_storage_details)
                provided_coverage = data.get("coverage", self.instance.coverage)
            else:
                existing_storage_details = provided_storage_details
                provided_coverage = data["coverage"]
            storage_details_validator = {
                models.Coverage.StorageType.FILESYSTEM: (
                    validators.GranuleFilesystemStorageDetailsCreate)
            }[provided_coverage.storage_type]
            try:
                storage_details_validator(**existing_storage_details)
            except ValidationError as exc:
                raise serializers.ValidationError(exc.json())
        provided_bbox_native = data.get("bbox_native")
        if provided_bbox_native is not None:
            if self.instance:
                provided_coverage = data.get(
                    "coverage", self.instance.coverage)
            else:
                provided_coverage = data["coverage"]
            bbox_native_geom = wkt.loads(provided_bbox_native)
            bbox_4326_geom = reproject_geometry(
                bbox_native_geom,
                int(provided_coverage.projection)
            )
            data["bbox_4326"] = wkt.dumps(bbox_4326_geom)
        return data


class SpatialDatasetSerializer(serializers.PolymorphicModelSerializer):
    polymorphic_serializers = [
        CoverageSerializer,
    ]

    class Meta:
        model = models.SpatialDataset
