# """Utilities for dealing with MapServer"""
#
# import collections.abc
# import logging
# import shlex
# import subprocess
# import typing
# import uuid
# from http.client import parse_headers
# from io import BytesIO
# from pathlib import Path
#
# import mappyfile
# from django.conf import settings
# from django.contrib.sites.models import Site
# from django.db import (
#     connection,
#     transaction
# )
# from django.urls import reverse
# from django.utils.text import slugify
#
# from . import models
#
#
# BAND_REGEXP = "^[0-9]{1,5}$"
# logger = logging.getLogger(__name__)
#
#
# def get_mapfile_path(workspace_name: str) -> Path:
#     dirname = f"mapfiles{'_debug' if settings.DEBUG else ''}"
#     return settings.DATA_ROOT / dirname / f"{workspace_name}.map"
#
#
# def generate_mapfile(workspace: models.Workspace) -> typing.Dict:
#     mapfile = get_mapfile_definition(workspace)
#     layer_handlers = {
#         models.Layer.LayerType.ISOLINE: (
#             generate_tile_index_mapfile_layer,
#             generate_mapfile_isoline_layer
#         ),
#         models.Layer.LayerType.RASTER: (
#             generate_tile_index_mapfile_layer,
#             generate_mapfile_raster_layer
#         ),
#         models.Layer.LayerType.VECTOR_FIELD: (
#             generate_tile_index_for_vector_field_layer,
#             generate_mapfile_vector_field_layer
#         ),
#     }
#     for layer in workspace.layers.all():
#         handlers = layer_handlers.get(layer.type)
#         if handlers is not None:
#             tileindex_handler, layer_handler = handlers
#             tileindex_definition = tileindex_handler(layer)
#             mapfile["layers"].append(tileindex_definition)
#             layer_definition = layer_handler(
#                 layer, workspace, tileindex_definition["name"])
#             mapfile["layers"].append(layer_definition)
#         else:
#             raise RuntimeError(f"Invalid layer type: {layer.type}")
#     return mapfile
#
#
# def save_mapfile(
#         mapfile_definition: typing.Dict,
#         workspace_name: str,
# ):
#     mapfile_path = get_mapfile_path(workspace_name)
#     mapfile_path.parent.mkdir(parents=True, exist_ok=True)
#     errors = mappyfile.validate(mapfile_definition)
#     if len(errors) == 0:
#         with mapfile_path.open("w", encoding="utf-8") as fh:
#             fh.write(mappyfile.dumps(mapfile_definition))
#         logger.info(f"mapfile_path: {mapfile_path!r}")
#     else:
#         raise RuntimeError(f"invalid mapfile: {errors}")
#     return mapfile_path
#
#
# def get_online_resource_url(workspace: models.Workspace):
#     protocol = "https" if settings.USE_SSL else "http"
#     domain = Site.objects.get_current().domain
#     relative_path = reverse(
#         "geospaca:ogc", kwargs={"workspace_name": workspace.name})
#     return f"{protocol}://{domain}{relative_path}?"
#
#
# def get_mapfile_definition(workspace: models.Workspace) -> typing.Dict:
#     additional_projections = workspace.additional_projections or []
#     advertised_projections = (
#         [workspace.default_projection] + additional_projections)
#     mapfile = {
#         "__type__": "map",
#     }
#     if settings.DEBUG:
#         mapfile["debug"] = 5
#         mapfile["config"] = {
#             "ms_errorfile": "/tmp/mapserver_errors.txt",
#             "cpl_debug": "on",
#             "proj_debug": "on",
#         }
#     if settings.MAPSERVER_FONTSET_PATH is not None:
#         mapfile["fontset"] = str(settings.MAPSERVER_FONTSET_PATH)
#     mapfile.update({
#         "name": workspace.name,
#         "projection": [
#             f"init=epsg:{workspace.default_projection}",
#         ],
#         "web": {
#             "__type__": "web",
#             "metadata": {
#                 "__type__": "metadata",
#                 "ows_title": workspace.ows_title,
#                 "ows_onlineresource": get_online_resource_url(workspace),
#                 "ows_enable_request": "*",
#                 "wms_feature_info_mime_type": "application/json",
#                 "ows_srs": " ".join(
#                     [f"EPSG:{proj}" for proj in advertised_projections])
#             }
#         },
#         "extent": list(workspace.bbox_native.extent),
#         "shapepath": str(settings.DATA_ROOT),
#         "imagecolor": [0, 0, 0],
#         "imagetype": "png",
#         "size": [800, 600],
#         "status": "on",
#         "outputformats": [
#             {
#                 "__type__": "outputformat",
#                 "name": "png",
#                 "mimetype": "image/png",
#                 "driver": "agg/png",
#                 "extension": "png",
#                 "imagemode": "rgb",
#                 "transparent": "false",
#             },
#         ],
#         "layers": []
#     })
#     symbols_dir = settings.MAPSERVER_SYMBOLS_DIR
#     logger.debug(f"symbols_dir: {symbols_dir}")
#     if symbols_dir is not None:
#         raw_symbol_definitions = get_symbol_definitions(symbols_dir)
#         logger.debug(f"raw_symbol_definitions: {raw_symbol_definitions}")
#         loaded_symbols = mappyfile.loads(raw_symbol_definitions)
#         logger.debug(f"loaded_symbols: {loaded_symbols}")
#         if len(loaded_symbols) > 0:
#             mapfile["symbols"] = loaded_symbols
#     return mapfile
#
#
# def get_symbol_definitions(symbols_dir: Path, encoding="utf-8") -> str:
#     result = ""
#     for path in symbols_dir.iterdir():
#         if path.is_file():
#             result = "\n".join((result, path.read_text(encoding=encoding)))
#         elif path.is_dir():
#             result = "\n".join((result, get_symbol_definitions(path)))
#     return result
#
#
# def get_layer_advertised_projections(
#         layer: models.Layer,
#         workspace: models.Workspace
# ) -> typing.List:
#     item_projection = layer.spatial_dataset.projection
#     additional = layer.advertised_projections or []
#     workspace_default = workspace.default_projection
#     workspace_additional = workspace.additional_projections or []
#     unique = set(
#         [item_projection]
#     ).union(
#         set(additional)
#     ).union(
#         [workspace_default]
#     ).union(
#         set(workspace_additional)
#     )
#     return unique
#
#
# def _generate_mapfile_layer_skeleton(
#         layer: models.Layer,
#         workspace: models.Workspace,
#         tileindex_name: typing.Optional[str] = None,
#         declare_dimensions: typing.Optional[typing.List] = None
# ) -> typing.Dict:
#
#     extent = layer.spatial_dataset.bbox_native.extent
#     advertised_projections = get_layer_advertised_projections(layer, workspace)
#     projections = " ".join(
#         f"EPSG:{srs.upper()}" for srs in advertised_projections)
#     layer_definition = {
#         "__type__": "layer",
#         "debug": 5 if settings.DEBUG else 0,
#         "name": layer.name,
#         "validation": {},
#         "projection": [
#             f"init=epsg:{layer.spatial_dataset.projection.lower()}"
#         ],
#         "extent": list(extent),
#         "metadata": {
#             "__type__": "metadata",
#             "ows_title": layer.name,
#             "ows_srs": projections,
#             "ows_include_items": "all",
#             "ows_extent": " ".join(str(i) for i in extent),
#             "gml_include_items": "all",
#             "wms_dimensionlist": ""
#         },
#         "status": "off",
#         "template": "templates/blank.json",
#         "units": "meters",  # FIXME
#     }
#     band_name = get_layer_band_name(layer.spatial_dataset)
#     layer_definition["validation"] = {
#         f"default_{band_name}": "1",
#         band_name: BAND_REGEXP,
#     }
#     if layer.style.type == models.Style.StyleType.MAPSERVER.value:
#         loaded = mappyfile.loads(layer.style.raw_value)
#         if isinstance(loaded, collections.abc.Sequence):
#             layer_definition.update(classes=loaded)
#         else:
#             layer_definition.update(classes=[loaded])
#     if tileindex_name is not None:
#         layer_definition.update(
#             tileindex=tileindex_name,
#             tileitem="location"
#         )
#     else:
#         layer_definition["data"] = "%data%"
#
#     dimensions = layer.spatial_dataset.dimensions.all()
#     if declare_dimensions is None:
#         dims_to_advertise = [d.name for d in dimensions]
#     else:
#         dims_to_advertise = list(declare_dimensions)
#     layer_definition["metadata"]["wms_dimensionlist"] = ", ".join(
#         d.name for d in dimensions if d.name in dims_to_advertise)
#     for dimension in dimensions:
#         if dimension.name not in dims_to_advertise:
#             continue
#         config_prefix = f"wms_{dimension.name}"
#         if dimension.name != "time":
#             # for some reason MapServer expects the metadata for the TIME
#             # dimension to be named like `wms_timeitem` while for all other
#             # dimensions, it expects something like `wms_elevation_item`
#             config_prefix += "_"
#         layer_definition["metadata"].update({
#             f"{config_prefix}item": dimension.name,
#             f"{config_prefix}units": dimension.units,
#             f"{config_prefix}extent": dimension.extent,
#             f"{config_prefix}default": dimension.default,
#         })
#     return layer_definition
#
#
# def get_layer_band_name(spatial_dataset: models.SpatialDataset):
#     slugified_id = slugify(str(spatial_dataset.id))
#     return f"band_{slugified_id}"
#
#
# def get_vector_field_layer_band_names(spatial_dataset: models.SpatialDataset):
#     base_name = get_layer_band_name(spatial_dataset)
#     return (
#         f"{base_name}_u",
#         f"{base_name}_v",
#     )
#
#
# def generate_mapfile_raster_layer(
#         layer: models.Layer,
#         workspace: models.Workspace,
#         tileindex_name: typing.Optional[str] = None,
# ) -> typing.Dict:
#     layer_definition = _generate_mapfile_layer_skeleton(
#         layer, workspace, tileindex_name)
#     band_name = get_layer_band_name(layer.spatial_dataset)
#     layer_definition.update({
#         "type": "raster",
#         "processing": [f"BANDS=%{band_name}%"],
#     })
#     return layer_definition
#
#
# def generate_mapfile_vector_field_layer(
#         layer: models.Layer,
#         workspace: models.Workspace,
#         tileindex_name: typing.Optional[str] = None,
# ) -> typing.Dict:
#     has_time_dim = "time" in [d.name for d in layer.spatial_dataset.dimensions]
#     declare_dims = ["time"] if has_time_dim else None
#     layer_definition = _generate_mapfile_layer_skeleton(
#         layer, workspace, tileindex_name, declare_dimensions=declare_dims)
#     u_band_name, v_band_name = get_vector_field_layer_band_names(
#         layer.spatial_dataset)
#     layer_definition.update({
#         "validation": {
#             f"default_{u_band_name}": "1",
#             u_band_name: BAND_REGEXP,
#             f"default_{v_band_name}": "2",
#             v_band_name: BAND_REGEXP,
#         },
#         "type": "point",
#         "connectiontype": "uvraster",
#         "processing": [
#             f"BANDS=%{u_band_name}%,%{v_band_name}%",
#             f"UV_SPACING={layer.details['spacing']}",
#             f"UV_SIZE_SCALE={layer.details['size_scale']}"
#         ]
#     })
#     return layer_definition
#
#
# # TODO: add support for smoothing preprocessing
# # TODO: add support for smoothing size
# def generate_mapfile_isoline_layer(
#         layer: models.Layer,
#         workspace: models.Workspace,
#         tileindex_name: typing.Optional[str] = None,
# ) -> typing.Dict:
#     layer_definition = _generate_mapfile_layer_skeleton(
#         layer, workspace, tileindex_name)
#     band_name = get_layer_band_name(layer.spatial_dataset)
#     processing_info = [
#         f"BANDS=%{band_name}%",
#         "LABEL_NO_CLIP=ON",
#         "CONTOUR_ITEM=elevation"
#     ]
#     interval = layer.details.get("interval")
#     fixed_levels = layer.details.get("fixed_levels")
#     if interval is not None:
#         processing_info.append(f"CONTOUR_INTERVAL={interval}")
#     elif fixed_levels is not None:
#         processing_info.append(f"CONTOUR_LEVELS={fixed_levels}")
#     layer_definition.update({
#         "type": "line",
#         "connectiontype": "contour",
#         "processing": processing_info,
#     })
#     generalization_algorithm = layer.details.get("generalization_algorithm")
#     if generalization_algorithm is not None:
#         layer_definition["geomtransform"] = _get_isoline_geomtransform(
#             generalization_algorithm,
#             layer.details.get("generalization_tolerance"),
#             smoothing_iteration=layer.details.get("smoothing_iterations")
#         )
#     return layer_definition
#
#
# def _get_isoline_geomtransform(
#         generalization_algorithm: str,
#         generalization_tolerance: float,
#         smoothing_size: typing.Optional[int] = 3,
#         smoothing_iteration: typing.Optional[int] = 1,
#         smoothing_preprocessing: typing.Optional[str] = None,
# ):
#     generalization_call = (
#         f"{generalization_algorithm}("
#         f"[shape], "
#         f"{generalization_tolerance}*[data_cellsize]"
#         f")"
#     )
#     smoothing_params = [
#         generalization_call,
#         str(smoothing_size),
#         str(smoothing_iteration)
#     ]
#     if smoothing_preprocessing is not None:
#         smoothing_params.append(f"{smoothing_preprocessing!r}")
#     smoothing_call = f"(smoothsia({', '.join(smoothing_params)}))"
#     return smoothing_call
#
#
# def get_tileindex_db_view_name(spatial_dataset: models.SpatialDataset):
#     return "_".join((
#         "tileindex",
#         slugify(str(spatial_dataset.id)).replace("-", "_"),
#     ))
#
#
# def complete_tile_index_layer_skeleton(
#         layer: models.Layer,
#         validation: typing.Dict,
#         processing: typing.List[str]
# ) -> typing.Dict:
#     db_view_name = get_tileindex_db_view_name(layer.spatial_dataset)
#     return {
#         "__type__": "layer",
#         "name": f"index_for_{layer.name}",
#         "debug": 5 if settings.DEBUG else 0,
#         "type": "polygon",
#         "connectiontype": "POSTGIS",
#         "connection": get_postgis_connection_string(),
#         "data": [
#             (
#                 f"geom FROM {db_view_name} "
#                 f"using unique id "
#                 f"using srid={layer.spatial_dataset.projection}"
#             )
#         ],
#         "metadata": {
#             "__type__": "metadata",
#             # this hides tileindex layer from GetCapabilities responses
#             "ows_enable_request": "!GetCapabilities"
#         },
#         "processing": processing,
#         "validation": validation
#     }
#
#
# def generate_tile_index_for_vector_field_layer(
#         layer: models.Layer
# ) -> typing.Dict:
#     """
#     Generate a tileindex layer definition suitable for vector field layers.
#     """
#
#     # geospaca implements vector field layers as Mapserver uvraster layers with
#     # a tileindex. Mapserver currently has some limitations regarding the usage
#     # of arbitrary dimensions on uvraster layers that have a tileindex. As such
#     # we use a temporary workaround:
#     # - the tileindex layer's VALIDATION treats all dimensions except TIME as
#     #   normal Mapserver variables that have runtime susbtitution applied to
#     #   them
#     # - we explicitly generate a FILTER (or more properly a
#     #   PROCESSING=NATIVE_FILTER) combining all dimensions except TIME in
#     #   order to compensate for mapserver's lack of automatically generating
#     #   such filter
#     dimensions = layer.spatial_dataset.dimensions.all()
#     validation_dim_prefixes = {
#         d.name: True if d.name != "time" else False for d in dimensions}
#     validation = build_tileindex_layer_validation(
#         dimensions,
#         use_dim_name_prefix=validation_dim_prefixes
#     )
#     native_filter = generate_native_filter_expression(dimensions)
#     processing_instructions = [
#         "CLOSE_CONNECTION=DEFER",
#         f"NATIVE_FILTER={native_filter}"
#     ]
#     return complete_tile_index_layer_skeleton(
#         layer, validation, processing_instructions)
#
#
# def generate_native_filter_expression(
#         dimensions: typing.Iterable[models.Dimension]) -> str:
#     parts = []
#     for dim in dimensions:
#         if dim.name == "time":
#             # mapserver UVRASTER has explicit support for TIME dimension when
#             # using a tileindex - it lacks support for other dimensions though
#             continue
#         parts.append(f"{dim.name}='%dim_{dim.name}%'")
#     return " AND ".join(parts)
#
#
# def generate_tile_index_mapfile_layer(layer: models.Layer) -> typing.Dict:
#     """Generate a mapfile LAYER to serve as a TILEINDEX for a raster layer.
#
#     For specifying a raster layer in a mapserver mapfile geospaca uses two
#     LAYER definitions - one of them is the actual layer and the other is used
#     by the first as a TILEINDEX. This allows complex filtering to be used in
#     order to perform dimensional subsetting of the layer (be it spatial,
#     temporal or other).
#
#     """
#
#     validation = build_tileindex_layer_validation(
#         layer.spatial_dataset.dimensions.all())
#     processing_instructions = [
#         "CLOSE_CONNECTION=DEFER",
#     ]
#     return complete_tile_index_layer_skeleton(
#         layer, validation, processing_instructions)
#
#
# def build_tileindex_layer_validation(
#         dimensions: typing.Iterable[models.Dimension],
#         use_dim_name_prefix: typing.Optional[typing.Dict[str, bool]] = None
# ) -> typing.Dict:
#     use_prefix = dict(use_dim_name_prefix) if use_dim_name_prefix else {}
#     result = {}
#     for dim in dimensions:
#         regexp = {
#             models.Dimension.DimensionType.TEMPORAL: (
#                 "^[0-9]{4}-[0-9]{2}-[0-9]{2}(T|\ [0-9]{2}:[0-9]{2}:[0-9]{2})?$"
#             ),
#             models.Dimension.DimensionType.NUMERICAL: "^[0-9]+(\.[0-9]+)?$",
#             models.Dimension.DimensionType.CATEGORICAL: "^.*$"
#         }[dim.type]
#         prefix = f"dim_" if use_prefix.get(dim.name, False) else ""
#         name = f"{prefix}{dim.name}"
#         result.update({
#             f"default_{name}": dim.default,
#             name: regexp
#         })
#     return result
#
#
# def get_postgis_connection_string():
#     db_info = settings.DATABASES["default"]
#     params = {
#         "user": db_info["USER"],
#         "password": db_info["PASSWORD"],
#         "host": db_info["HOST"],
#         "port": db_info["PORT"],
#         "dbname": db_info["NAME"],
#     }
#     return " ".join(f"{k}={v}" for k, v in params.items())
#
#
# def call_mapserv(
#         query_params: typing.Dict
# ) -> typing.Tuple[typing.Mapping[str, str], bytes, str]:
#     query_string = _serialize_mapserv_payload(**query_params)
#     logger.debug(f"query_string: {query_string}")
#     completed_mapserv = subprocess.run(
#         shlex.split(f"{settings.MAPSERV_PATH} QUERY_STRING='{query_string}'"),
#         stdin=subprocess.PIPE,
#         stdout=subprocess.PIPE,
#         stderr=subprocess.PIPE
#     )
#     raw_headers, body = completed_mapserv.stdout.partition(b"\r\n\r\n")[::2]
#     headers = parse_headers(BytesIO(raw_headers))
#     return headers, body, str(completed_mapserv.stderr, "utf-8")
#
#
# def _serialize_mapserv_payload(**payload):
#     return "&".join([f"{k}={str(v)}" for k, v in payload.items()])
#
# def regenerate_coverage_tileindex_view(coverage_id: str) -> str:
#     """"(Re)generate the DB view that is used by mapserver as a tileindex.
#
#     This function updates the DB view that is used as a tileindex in
#     mapserver mapfiles. The update process entails the following steps:
#
#     * Remove a possibly pre-existing view;
#     * Create a new view;
#     * Populate the view with data;
#
#     """
#
#     coverage = models.Coverage.objects.get(pk=uuid.UUID(coverage_id))
#     db_view_name = get_tileindex_db_view_name(coverage)
#     logger.info(f"db_view_name: {db_view_name}")
#     with connection.cursor() as db_cursor:
#         with transaction.atomic():
#             drop_coverage_tileindex_view(db_view_name, db_cursor)
#             create_coverage_tileindex_view(db_view_name, coverage, db_cursor)
#             refresh_coverage_tileindex_view(db_view_name, db_cursor)
#         db_cursor.execute(f"VACUUM ANALYZE {db_view_name}")
#     return db_view_name
#
#
# @transaction.atomic
# def create_coverage_tileindex_view(
#         view_name: str,
#         coverage: models.Coverage,
#         cursor
# ):
#     view_generation_ddl = _generate_create_view_query(view_name, coverage)
#     logger.info(f"ddl: {view_generation_ddl}")
#     cursor.execute(view_generation_ddl)
#     cursor.execute(
#         f"CREATE UNIQUE INDEX ON {view_name} (id)"
#     )
#     gist_index_name = f"{view_name}_geom_gist_idx"
#     cursor.execute(
#         f"CREATE INDEX {gist_index_name} ON {view_name} "
#         f"USING GIST (geom gist_geometry_ops_nd)"
#     )
#     # create a multi-column index for the dimensions
#     dimensions = coverage.dimensions.all()
#     dim_index_name = f"{view_name}_{'_'.join(d.name for d in dimensions)}_idx"
#     cursor.execute(
#         f"CREATE INDEX {dim_index_name} ON {view_name} ("
#         f"{', '.join(d.name for d in dimensions)})"
#     )
#
#
# def drop_coverage_tileindex_view(view_name, cursor):
#     return cursor.execute(f"DROP MATERIALIZED VIEW IF EXISTS {view_name}")
#
#
# def refresh_coverage_tileindex_view(
#         view_name: str,
#         cursor,
#         concurrently: typing.Optional[bool] = False
# ):
#     return cursor.execute(
#         f"REFRESH MATERIALIZED VIEW"
#         f" {'CONCURRENTLY' if concurrently else ''} {view_name}"
#     )
#
#
# def _prepare_tileindex_dimension_values_columns(
#         coverage: models.Coverage) -> typing.List[typing.Tuple[str, str]]:
#     columns = []
#     for dim in coverage.dimensions.all():
#         if dim.type == models.Dimension.DimensionType.TEMPORAL:
#             column_ddl = f"to_timestamp(dimension_values->>'{dim.name}', 'YYYY-MM-DD HH24:MI:SS')"
#         elif dim.type == models.Dimension.DimensionType.NUMERICAL:
#             column_ddl = f"CAST(dimension_values->>'{dim.name}' AS float)"
#         else:
#             column_ddl = f"dimension_values->>'{dim.name}'"
#         columns.append((column_ddl, dim.name))
#     return columns
#
#
# def _generate_create_view_query(
#         name: str,
#         coverage: models.Coverage
# ) -> str:
#     """Generate SQL DDL commands for creating a view for a coverage's tileindex
#
#     This function outputs the textual SQL that can be sent to the DB in order
#     to request the creation of a materialized view.
#
#     Note that the generated SQL includes the `WITH NO DATA` fragment, which
#     means that when it is executed, the corresponding materialized view is
#     created but it will not be immediately populated.
#
#     """
#     location_column_ddl = _prepare_tileindex_location_column(coverage)
#     dimension_columns_ddl = _prepare_tileindex_dimension_values_columns(coverage)
#     dimensions_ddl = ", ".join([f"{val} AS {alias}" for val, alias in dimension_columns_ddl])
#     query = f"""
#     CREATE MATERIALIZED VIEW {name} AS (
#         SELECT
#             id,
#             {location_column_ddl} AS location,
#             ST_SetSRID(bbox_native, {coverage.projection})::geometry(POLYGON, {coverage.projection}) AS geom,
#             {dimensions_ddl}
#         FROM geospaca_granule
#         WHERE coverage_id = '{coverage.id}'
#     )
#     WITH NO DATA
#     """
#     return query
#
#
# def _prepare_tileindex_location_column(spatial_dataset: models.SpatialDataset):
#     # depending on the coverage's storage_type, build the appropriate column
#     if spatial_dataset.storage_type == models.SpatialDataset.StorageType.FILESYSTEM:
#         result = _prepare_filesystem_location_column(spatial_dataset)
#     else:
#         raise NotImplementedError
#     return result
#
#
# def _prepare_filesystem_location_column(
#         spatial_dataset: models.SpatialDataset,
# ):
#     storage_config = spatial_dataset.storage_configuration
#     result_fragments = []
#     for structure_item in storage_config["structure"]:
#         dimension = spatial_dataset.dimensions.get(
#             name=structure_item.dimension)
#         if dimension.type == models.Dimension.DimensionType.TEMPORAL:
#             path_fragment = _prepare_column_fragment_temporal_dim(
#                 dimension.name, structure_item.temporal_fragments)
#             result_fragments.append(path_fragment)
#         elif dimension.type == models.Dimension.DimensionType.NUMERICAL:
#             raise NotImplementedError
#         elif dimension.type == models.Dimension.DimensionType.CATEGORICAL:
#             raise NotImplementedError
#         else:
#             raise NotImplementedError
#     return _prepare_column_ddl(
#         storage_config["root_data_dir"],
#         filename=f"storage_details->>'filename'",
#         path_fragments=result_fragments
#     )
#
#
# def _prepare_column_ddl(
#         base_dir: str,
#         filename: str,
#         path_fragments: typing.Optional[typing.List[str]] = None
# ):
#     column_ddl = f"CONCAT_WS('/', '{base_dir}'"
#     if len(path_fragments or []) > 0:
#         joined_fragments = ", ".join(f"'{frag}'" for frag in path_fragments)
#         column_ddl = ", ".join((column_ddl, joined_fragments))
#     column_ddl += f", {filename})"
#     print(f"column_ddl: {column_ddl}")
#     return column_ddl
#
#
# def _prepare_column_fragment_temporal_dim(
#         dimension: str,
#         temporal_fragments: typing.List[str]):
#     items = []
#     for fragment in temporal_fragments:
#         timestamp_item = (
#             f"to_timestamp("
#             f"dimension_values->>'{dimension}', "
#             f"'YYYY-MM-DD HH24:MI:SS'"
#             f")"
#         )
#         part_fragment = f"EXTRACT({fragment.upper()} FROM {timestamp_item})"
#         if fragment in ("month", "day", "hour", "minute", "second"):
#             part_fragment = f"lpad({part_fragment}::text, 2, '0')"
#         part_fragment = f"'{part_fragment}'"
#         items.append(part_fragment)
#     joined_items = ", ".join(items) if len(items) > 0 else "''"
#     return f"CONCAT_WS('/', {joined_items})"
