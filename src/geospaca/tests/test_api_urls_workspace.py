import typing

import pytest
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.test import APIClient

from .. import models

pytestmark = pytest.mark.django_db(transaction=True)
ENDPOINT_BASE = "/api/workspaces/"
ExistingDbContentsType = typing.Dict[str, typing.List[typing.Dict]]


@pytest.mark.parametrize("payload", [
    pytest.param(
        {
            "data": {
                "type": "Workspace",
                "attributes": {
                    "name": "dummy",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                },
            },
        },
        id="only required fields"
    )
])
def test_create_workspace(authenticated_api_client: APIClient, payload: typing.Dict):
    encoder = DjangoJSONEncoder()
    response = authenticated_api_client.post(
        ENDPOINT_BASE,
        data=encoder.encode(payload),
        content_type="application/vnd.api+json"
    )
    assert response.status_code == 201


@pytest.mark.parametrize("existing", [
    pytest.param(
        {},
        id="without pre-existing workspaces"
    ),
    pytest.param(
        {
            "workspaces": [
                {"name": "dummy", "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))"},
            ]
        },
        id="with a single workspace"
    ),
    pytest.param(
        {
            "workspaces": [
                {"name": "first dummy", "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))"},
                {"name": "second dummy", "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))"},
            ]
        },
        id="with two workspaces"
    ),
])
def test_list_workspaces(
        api_client: APIClient,
        populate_db: typing.Callable,
        existing: typing.Dict[str, typing.List]
):
    populate_db(**existing)
    response = api_client.get(ENDPOINT_BASE)
    assert response.status_code == 200
    contents = response.json()
    assert len(contents["data"]) == len(existing.get("workspaces", []))


@pytest.mark.xfail(raises=NotImplementedError)
def test_list_workspaces_with_pagination():
    raise NotImplementedError


@pytest.mark.parametrize("existing, test_index", [
    pytest.param(
        {
            "workspaces": [
                {"name": "dummy", "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))"},
            ]
        },
        0,
        id="single workspace"
    )
])
def test_get_workspace_detail(
        api_client: APIClient,
        populate_db: typing.Callable,
        existing: typing.Dict[str, typing.List],
        test_index: int
):
    records = populate_db(**existing)
    workspace_under_test: models.Workspace = records["workspaces"][test_index]
    api_endpoint = f"{ENDPOINT_BASE}{workspace_under_test.pk}/"
    response = api_client.get(api_endpoint)
    assert response.status_code == 200
    contents = response.json()
    assert contents["data"]["id"] == str(workspace_under_test.id)
    assert contents["data"]["attributes"]["name"] == workspace_under_test.name
    assert contents["data"]["attributes"]["bbox_native"] == workspace_under_test.bbox_native
    assert contents["data"]["links"]["self"].endswith(api_endpoint)
    assert contents["data"]["relationships"]["layers"].get("data") is None
    relationship_link = (
        contents["data"]["relationships"]["layers"]["links"]["self"])
    related_link = (
        contents["data"]["relationships"]["layers"]["links"]["related"])
    assert relationship_link.endswith(f"{api_endpoint}relationships/layers/")
    assert related_link.endswith(f"{api_endpoint}layers/")


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_workspace_related_layers():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_workspace_relationships_layers():
    raise NotImplementedError


@pytest.mark.parametrize("existing, delete_index", [
    pytest.param(
        {
            "workspaces": [
                {"name": "dummy", "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))"},
            ]
        },
        0,
    )
])
def test_delete_workspace(
        authenticated_api_client: APIClient,
        populate_db: typing.Callable,
        existing: ExistingDbContentsType,
        delete_index: int
):
    records = populate_db(**existing)
    workspace_under_test = records["workspaces"][delete_index]
    workspace_id = workspace_under_test.id
    response = authenticated_api_client.delete(f"{ENDPOINT_BASE}{workspace_id}/")
    assert response.status_code == 204
    with pytest.raises(models.Workspace.DoesNotExist):
        models.Workspace.objects.get(pk=workspace_id)


@pytest.mark.xfail(raises=NotImplementedError)
def test_create_workspace_relationship_layer():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_remove_workspace_relationship_layer():
    raise NotImplementedError
