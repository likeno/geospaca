import typing

import pytest
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.test import APIClient

from .. import models

pytestmark = pytest.mark.django_db(transaction=True)
ENDPOINT_BASE = "/api/dimensions/"
ExistingDbContentsType = typing.Dict[str, typing.List[typing.Dict]]


@pytest.mark.parametrize(
    "existing, base_payload, relate_to_coverage_index, expected_code",
    [
        pytest.param(
            None,
            {
                "data": {
                    "type": "Dimension",
                    "attributes": {
                        "name": "dummy dimension",
                        "type": "categorical",
                    },
                },
            },
            None,
            400,
            id="bare dimension, without relationships"
        ),
        pytest.param(
            {
                "coverages": [
                    {
                        "name": "dummy coverage",
                        "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                    }
                ]
            },
            {
                "data": {
                    "type": "Dimension",
                    "attributes": {
                        "name": "dummy dimension",
                        "type": "categorical",
                    },
                },
            },
            0,
            201,
            id="simple dimension, related to pre-existing coverage"
        ),
    ]
)
def test_create_dimension(
        authenticated_api_client: APIClient,
        populate_db: typing.Callable,
        existing: typing.Optional[ExistingDbContentsType],
        base_payload: typing.Dict,
        relate_to_coverage_index: int,
        expected_code: int,

):
    if existing is not None:
        records = populate_db(**existing)
    else:
        records = {}
    payload = base_payload.copy()
    if relate_to_coverage_index is not None:
        coverage = records["coverages"][relate_to_coverage_index]
        payload["data"]["relationships"] = {
            "spatial_dataset": {
                "data": {
                    "type": "SpatialDataset",
                    "id": str(coverage.id)
                },
            }
        }
    print(f"payload: {payload}")
    encoder = DjangoJSONEncoder()
    response = authenticated_api_client.post(
        ENDPOINT_BASE,
        data=encoder.encode(payload),
        content_type="application/vnd.api+json"
    )
    print(f"response: {response.json()}")
    assert response.status_code == expected_code
    if expected_code == 201:
        dim_id = response.json()["data"]["id"]
        models.Dimension.objects.get(pk=dim_id)


@pytest.mark.parametrize("existing", [
    pytest.param(
        {},
        id="without pre-existing dimensions"
    ),
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage1",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                    "dimensions": [
                        {
                            "name": "dummy dimension1",
                            "type": "categorical",
                        },
                    ]
                },
            ]
        },
        id="with a single dimension"
    ),
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage1",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                    "dimensions": [
                        {
                            "name": "dummy dimension1",
                            "type": "categorical",
                        },
                        {
                            "name": "dummy dimension2",
                            "type": "categorical",
                        },
                    ]
                },
            ]
        },
        id="with two dimensions"
    ),
])
def test_list_dimensions(
        populate_db: typing.Callable,
        api_client: APIClient,
        existing: typing.Dict[str, typing.List]
):
    populate_db(**existing)
    response = api_client.get(ENDPOINT_BASE)
    assert response.status_code == 200
    contents = response.json()
    print(f"contents: {contents}")
    num_dims = sum([
        len(cov.get("dimensions", [])) for cov in existing.get("coverages", [])
    ])
    print(f"num_dims: {num_dims}")
    total = 0
    for cov in existing.get("coverages", []):
        total += len(cov.get("dimensions", []))
    print(f"total: {total}")

    assert len(contents["data"]) == num_dims


@pytest.mark.xfail(raises=NotImplementedError)
def test_list_coverages_with_pagination():
    raise NotImplementedError


@pytest.mark.parametrize("existing, test_index", [
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage1",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                    "dimensions": [
                        {
                            "name": "dummy dimension1",
                            "type": "categorical",
                        },
                    ]
                },
            ]
        },
        0,
        id="single coverage"
    )
])
def test_get_dimension_detail(
        api_client: APIClient,
        populate_db: typing.Callable,
        existing: typing.Dict[str, typing.List],
        test_index: int
):
    records = populate_db(**existing)
    dimension_under_test: models.Dimension = records["dimensions"][test_index]
    api_endpoint = f"{ENDPOINT_BASE}{dimension_under_test.pk}/"
    response = api_client.get(api_endpoint)
    assert response.status_code == 200
    contents = response.json()
    assert contents["data"]["id"] == str(dimension_under_test.id)
    assert contents["data"]["attributes"]["name"] == dimension_under_test.name
    assert contents["data"]["links"]["self"].endswith(api_endpoint)

    assert contents["data"]["relationships"]["spatial_dataset"].get("data") is None

    spatial_dataset_relationship_link = (
        contents["data"]["relationships"]["spatial_dataset"]["links"]["self"])
    spatial_dataset_related_link = (
        contents["data"]["relationships"]["spatial_dataset"]["links"]["related"])
    assert spatial_dataset_relationship_link.endswith(f"{api_endpoint}relationships/spatial_dataset/")
    assert spatial_dataset_related_link.endswith(f"{api_endpoint}spatial_dataset/")


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_dimension_related_spatial_dataset():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_dimension_relationships_spatial_dataset():
    raise NotImplementedError


@pytest.mark.parametrize("existing, delete_index", [
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage1",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                    "dimensions": [
                        {
                            "name": "dummy dimension1",
                            "type": "categorical",
                        },
                    ]
                },
            ]
        },
        0,
    )
])
@pytest.mark.xfail(raises=NotImplementedError)
def test_delete_dimension(
        authenticated_api_client: APIClient,
        populate_db: typing.Callable,
        existing: ExistingDbContentsType,
        delete_index: int
):
    raise NotImplementedError
    # records = populate_db(**existing)
    # dimension_under_test = records["dimensions"][delete_index]
    # dimension_id = dimension_under_test.id
    # response = authenticated_api_client.delete(f"{ENDPOINT_BASE}{dimension_id}/")
    # assert response.status_code == 204


# These last 2 tests, when they will be implemented, are supposed to fail
# as we must not be allowed to modify the 1:1 relationship between dimension
# and spatial_dataset - at least not on the dimension side

@pytest.mark.xfail(raises=NotImplementedError)
def test_create_dimension_relationship_spatial_dataset():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_remove_dimension_relationship_spatial_dataset():
    raise NotImplementedError
