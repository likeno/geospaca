import typing

import pytest
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.test import APIClient

from .. import models

pytestmark = pytest.mark.django_db(transaction=True)
ENDPOINT_BASE = "/api/coverages/"
ExistingDbContentsType = typing.Dict[str, typing.List[typing.Dict]]


@pytest.mark.parametrize("payload, expected_code", [
    pytest.param(
        {
            "data": {
                "type": "Coverage",
                "attributes": {
                    "name": "dummy name",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                },
            },
        },
        201,
        id="simple coverage"
    ),
    pytest.param(
        {
            "data": {
                "type": "Coverage",
                "attributes": {
                    "name": "dummy name",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                },
            },
        },
        201,
        id="coverage related to a response_handler"
    ),
])
def test_create_coverage(
        authenticated_api_client: APIClient,
        payload: typing.Dict,
        expected_code: int
):
    encoder = DjangoJSONEncoder()
    response = authenticated_api_client.post(
        ENDPOINT_BASE,
        data=encoder.encode(payload),
        content_type="application/vnd.api+json"
    )
    print(response.json())
    assert response.status_code == expected_code


@pytest.mark.parametrize("existing", [
    pytest.param(
        {},
        id="without pre-existing coverages"
    ),
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                },
            ]
        },
        id="with a single coverage"
    ),
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage1",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                },
                {
                    "name": "dummy coverage2",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                },
            ]
        },
        id="with two coverages"
    ),
])
def test_list_coverages(
        populate_db: typing.Callable,
        api_client: APIClient,
        existing: typing.Dict[str, typing.List]
):
    populate_db(**existing)
    response = api_client.get(ENDPOINT_BASE)
    assert response.status_code == 200
    contents = response.json()
    assert len(contents["data"]) == len(existing.get("coverages", []))


@pytest.mark.xfail(raises=NotImplementedError)
def test_list_coverages_with_pagination():
    raise NotImplementedError


@pytest.mark.parametrize("existing, test_index", [
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                }
            ]
        },
        0,
        id="single coverage"
    )
])
def test_get_coverage_detail(
        api_client: APIClient,
        populate_db: typing.Callable,
        existing: typing.Dict[str, typing.List],
        test_index: int
):
    records = populate_db(**existing)
    coverage_under_test: models.Coverage = records["coverages"][test_index]
    api_endpoint = f"{ENDPOINT_BASE}{coverage_under_test.pk}/"
    response = api_client.get(api_endpoint)
    assert response.status_code == 200
    contents = response.json()
    assert contents["data"]["id"] == str(coverage_under_test.id)
    assert contents["data"]["attributes"]["name"] == coverage_under_test.name
    assert contents["data"]["links"]["self"].endswith(api_endpoint)

    assert contents["data"]["relationships"]["dimensions"].get("data") is None
    assert contents["data"]["relationships"]["layers"].get("data") is None
    assert contents["data"]["relationships"]["granules"].get("data") is None

    dimensions_relationship_link = (
        contents["data"]["relationships"]["dimensions"]["links"]["self"])
    dimensions_related_link = (
        contents["data"]["relationships"]["dimensions"]["links"]["related"])
    assert dimensions_relationship_link.endswith(f"{api_endpoint}relationships/dimensions/")
    assert dimensions_related_link.endswith(f"{api_endpoint}dimensions/")

    layers_relationship_link = (
        contents["data"]["relationships"]["layers"]["links"]["self"])
    layers_related_link = (
        contents["data"]["relationships"]["layers"]["links"]["related"])
    assert layers_relationship_link.endswith(f"{api_endpoint}relationships/layers/")
    assert layers_related_link.endswith(f"{api_endpoint}layers/")

    granules_relationship_link = (
        contents["data"]["relationships"]["granules"]["links"]["self"])
    granules_related_link = (
        contents["data"]["relationships"]["granules"]["links"]["related"])
    assert granules_relationship_link.endswith(f"{api_endpoint}relationships/granules/")
    assert granules_related_link.endswith(f"{api_endpoint}granules/")


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_coverage_related_dimensions():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_coverage_relationships_dimensions():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_coverage_related_layers():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_coverage_relationships_layers():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_coverage_related_granules():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_get_coverage_relationships_granules():
    raise NotImplementedError


@pytest.mark.parametrize("existing, delete_index", [
    pytest.param(
        {
            "coverages": [
                {
                    "name": "dummy coverage",
                    "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))",
                }
            ]
        },
        0,
    )
])
def test_delete_coverage(
        authenticated_api_client: APIClient,
        populate_db: typing.Callable,
        existing: ExistingDbContentsType,
        delete_index: int
):
    records = populate_db(**existing)
    coverage_under_test = records["coverages"][delete_index]
    coverage_id = coverage_under_test.id
    response = authenticated_api_client.delete(
        f"{ENDPOINT_BASE}{coverage_id}/")
    assert response.status_code == 204


@pytest.mark.xfail(raises=NotImplementedError)
def test_create_coverage_relationship_dimension():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_remove_coverage_relationship_dimension():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_create_coverage_relationship_layer():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_remove_coverage_relationship_layer():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_create_coverage_relationship_granule():
    raise NotImplementedError


@pytest.mark.xfail(raises=NotImplementedError)
def test_remove_coverage_relationship_granule():
    raise NotImplementedError
