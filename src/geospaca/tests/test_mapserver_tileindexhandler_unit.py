import pytest

pytestmark = pytest.mark.unit

from ..mapserver import tileindexhandler


@pytest.mark.parametrize("dimension, fragments, expected", [
    pytest.param(
        "time",
        [],
        "CONCAT_WS('/', '')",
        id="without any fragments"
    ),
    pytest.param(
        "time",
        ["year"],
        "CONCAT_WS('/', EXTRACT(YEAR FROM to_timestamp(dimension_values->>'time', 'YYYY-MM-DD HH24:MI:SS')))",
        id="single year fragment"
    ),
    pytest.param(
        "time",
        ["month"],
        "CONCAT_WS('/', lpad(EXTRACT(MONTH FROM to_timestamp(dimension_values->>'time', 'YYYY-MM-DD HH24:MI:SS'))::text, 2, '0'))",
        id="single month fragment"
    ),
    pytest.param(
        "time",
        ["year", "month"],
        (
                "CONCAT_WS("
                "'/', "
                "EXTRACT(YEAR FROM to_timestamp(dimension_values->>'time', 'YYYY-MM-DD HH24:MI:SS')), "
                "lpad(EXTRACT(MONTH FROM to_timestamp(dimension_values->>'time', 'YYYY-MM-DD HH24:MI:SS'))::text, 2, '0')"
                ")"
        ),
        id="year and month fragments"
    ),
])
def test_prepare_column_fragment_temporal_dim(dimension, fragments, expected):
    result = tileindexhandler._prepare_column_fragment_temporal_dim(
        dimension, fragments)
    assert result == expected


@pytest.mark.parametrize("base, filename, fragments, expected", [
    pytest.param(
        "dummy_base",
        "dummy_filename",
        [],
        "CONCAT_WS('/', 'dummy_base', dummy_filename)",
        id="without any fragments"
    ),
    pytest.param(
        "dummy_base",
        "dummy_filename",
        ["first_fragment"],
        "CONCAT_WS('/', 'dummy_base', first_fragment, dummy_filename)",
        id="with a single fragment"
    ),
    pytest.param(
        "dummy_base",
        "dummy_filename",
        ["first_fragment", "second_fragment"],
        "CONCAT_WS('/', 'dummy_base', first_fragment, second_fragment, dummy_filename)",
        id="with two fragments"
    ),
])
def test_prepare_column_ddl(base, filename, fragments, expected):
    result = tileindexhandler._prepare_column_ddl(base, filename, fragments)
    assert result == expected
