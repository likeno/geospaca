import re

import pytest

from ..mapserver import mapfilehandler

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("value", [
    pytest.param("2020-01-01"),
    pytest.param("2020-01-01 00:00:00"),
    pytest.param("2020-01-01T00:00:00"),
])
def test_temporal_validation_parameter(value: str):
    assert re.search(mapfilehandler.TEMPORAL_REGEXP, value) is not None
