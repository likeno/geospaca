import typing

import pytest

from ..api import serializers


@pytest.mark.parametrize("data", [
    pytest.param(
        {
            "name": "dummy",
            "bbox_native": "POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))"
        },
        id="only required fields"
    )
])
@pytest.mark.django_db
def test_workspace_serializer(data: typing.Dict):
    serializer = serializers.WorkspaceSerializer(data=data)
    assert serializer.is_valid()