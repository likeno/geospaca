from copy import deepcopy
import typing

import pytest
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

from ..api import serializers


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "integration: run only integration tests"
    )
    config.addinivalue_line(
        "markers",
        "functional: run only functional tests"
    )
    config.addinivalue_line(
        "markers",
        "unit: run only unit tests"
    )


@pytest.fixture
def api_client():
    client = APIClient()
    return client


@pytest.fixture
def user():
    user_model = get_user_model()
    some_user = user_model(username="tester")
    return some_user


@pytest.fixture
def authenticated_api_client(api_client, user):
    api_client.force_authenticate(user)
    yield api_client
    api_client.force_authenticate(None)


@pytest.fixture
def populate_db():
    return _populate_db


def _populate_db(
        coverages: typing.Optional[typing.List[typing.Dict]] = None,
        workspaces: typing.Optional[typing.List[typing.Dict]] = None,
) -> typing.Dict[str, typing.List]:
    result = {
        "coverages": [],
        "dimensions": [],
        "granules": [],
        "workspaces": [],
    }
    # we do a deepcopy because later below we start `pop`ing items from the
    # included dictionaries
    coverage_definitions = deepcopy(coverages) if coverages is not None else []
    workspace_definitions = (
        deepcopy(workspaces) if workspaces is not None else [])
    for coverage_definition in coverage_definitions:
        dimensions = coverage_definition.pop("dimensions", [])
        granules = coverage_definition.pop("granules", [])
        coverage_serializer = serializers.CoverageSerializer(
            data=coverage_definition, context={"request": None})
        coverage_serializer.is_valid(raise_exception=True)
        coverage = coverage_serializer.save()
        for dimension_definition in dimensions:
            dimension_definition.update(
                spatial_dataset={"type": "Coverage", "id": coverage.id}
            )
            dimension_serializer = serializers.DimensionSerializer(
                data=dimension_definition)
            dimension_serializer.is_valid(raise_exception=True)
            dimension = dimension_serializer.save()
            result["dimensions"].append(dimension)
        for granule_definition in granules:
            granule_definition.update(coverage=coverage.id)
            serializer = serializers.GranuleSerializer(data=granule_definition)
            serializer.is_valid(raise_exception=True)
            granule = serializer.save()
            result["granules"].append(granule)
        result["coverages"].append(coverage)
    for workspace_definition in workspace_definitions:
        serializer = serializers.WorkspaceSerializer(data=workspace_definition)
        serializer.is_valid(raise_exception=True)
        workspace = serializer.save()
        result["workspaces"].append(workspace)
    return result
