from enum import Enum
import typing

import typing_extensions
from django.conf import settings
from django.core.exceptions import ValidationError
from pydantic import (
    BaseModel,
    PositiveInt,
    UUID4,
    validator,
)

from .models import SpatialDataset


class GeneralizationAlgorithm(Enum):
    THIN_NO_POINT = "generalize"
    DOUGLAS_PEUCKER = "simplify"


class StorageStructureTemporalDimension(BaseModel):
    dimension: str
    temporal_fragments: typing.List[
        typing_extensions.Literal[
            "year",
            "month",
            "day",
            "hour",
            "minute",
            "second"
        ]
    ] = ["year", "month", "day"]


class NumericalStorageRange(BaseModel):
    start: int
    end: int
    step: int


class StorageStructureNumericalDimension(BaseModel):
    dimension: str
    range: NumericalStorageRange


class StorageStructureCategoricalDimension(BaseModel):
    dimension: str
    storage_arguments: typing.Optional[str]


class FilesystemStorageConfiguration(BaseModel):
    root_data_dir: typing.Optional[str] = str(settings.DATA_ROOT)
    structure: typing.Optional[
        typing.List[
            typing.Union[
                StorageStructureTemporalDimension,
                StorageStructureNumericalDimension,
                StorageStructureCategoricalDimension
            ]
        ]
    ]

    @validator("structure", always=True)
    def provide_default_structure(cls, value):
        return value if value is not None else []


class StorageConfiguration(BaseModel):
    storage_type: SpatialDataset.StorageType = SpatialDataset.StorageType.FILESYSTEM
    configuration: typing.Optional[
        typing.Union[
            FilesystemStorageConfiguration
        ]
    ] = FilesystemStorageConfiguration()


class GranuleFilesystemStorageDetailsCreate(BaseModel):
    filename: str
    band: typing.Optional[PositiveInt] = 1
    subdataset_prefix: typing.Optional[str]
    subdataset_suffix: typing.Optional[str]


def validate_storage_configuration(value: typing.Dict):
    conf = StorageConfiguration(**value)


class RasterLayerDetails(BaseModel):
    pass


class VectorLayerDetails(BaseModel):
    pass


class IsolineLayerDetails(VectorLayerDetails):
    interval: typing.Optional[int]
    fixed_levels: typing.Optional[typing.List[int]]
    generalization_algorithm: typing.Optional[str]
    generalization_tolerance: typing.Optional[float]
    smoothing_iterations: typing.Optional[int] = 1

    @validator("fixed_levels", always=True, pre=True, whole=True)
    def ensure_either_interval_or_levels(cls, levels, values):
        message = "Supply one of fixed_levels or interval"
        print(f"levels: {levels}")
        print(f"values: {values}")
        interval = values.get("interval")
        if levels is not None and interval is not None:
            raise ValueError(message)
        elif levels is None and interval is None:
            raise ValueError(message)
        return levels

    @validator("fixed_levels", whole=True)
    def ensure_at_least_one_level(cls, levels):
        if levels is not None and len(levels) == 0:
            raise ValueError("Must supply at least one level")
        return levels

    @validator("interval")
    def ensure_interval_greater_than_zero(cls, interval):
        if interval <= 0:
            raise ValueError("Must supply a positive interval")
        return interval

    @validator("generalization_algorithm")
    def ensure_valid_generalization_algorithm(cls, value):
        if value is not None:
            GeneralizationAlgorithm(value)
        return value

    @validator("generalization_tolerance", always=True)
    def ensure_tolerance_if_algorithm_has_been_specified(
            cls, tolerance, values):
        if values.get("generalization_algorithm") is not None:
            if tolerance is None:
                raise ValueError(
                    "Tolerance must be specified whenever a generalization "
                    "algorithm is in use"
                )
        return tolerance


class VectorFieldLayerDetails(VectorLayerDetails):
    spacing: typing.Optional[int] = 32
    size_scale: typing.Optional[float] = 1
