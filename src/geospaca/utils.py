import importlib
import typing


def lazy_import(python_path: str) -> typing.Callable:
    """Import a Python named object dynamically."""
    module_path, python_name = python_path.rpartition(".")[::2]
    loaded_module = importlib.import_module(module_path)
    return getattr(loaded_module, python_name)
