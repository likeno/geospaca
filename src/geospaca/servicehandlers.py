from dataclasses import dataclass
import datetime as dt
import logging
import typing

import dateutil.parser
from django.db import connection
from django.http import (
    Http404,
    HttpRequest,
    HttpResponse,
)

from . import models
from .mapserver import (
    mapfilehandler,
    mapservhandler,
)

logger = logging.getLogger(__name__)


@dataclass
class RequestedDimension(object):
    dimension: models.Dimension
    requested_name: str
    raw_value: str
    parsed_values: typing.List[
        typing.Tuple[
            typing.Union[dt.datetime, float, str],
            typing.Optional[
                typing.Union[dt.datetime, float, str]
            ]
        ]
    ]


class BaseHandler:

    def handle_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        raise NotImplementedError


class BaseWmsVersion130Handler(BaseHandler):
    """A handler for OGC WMS v1.3.0 requests

    It relies mostly on mapserver for the actual request processing

    """

    def handle_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        query_parameters.setdefault("version", "1.3.0")
        query_parameters.setdefault("request", "GetCapabilities")
        if query_parameters["version"] != "1.3.0":
            raise Http404("Invalid WMS version")
        handler = {
            "GetCapabilities": self.handle_get_capabilities_request,
            "GetLegendGraphic": self.handle_get_legend_graphic_request,
            "GetMap": self.handle_get_map_request,
            "GetFeatureInfo": self.handle_get_feature_info_request,
        }[query_parameters["request"]]
        return handler(workspace, query_parameters, request)

    def handle_get_capabilities_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        return process_mapserv_request(workspace, query_parameters)

    def handle_get_legend_graphic_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        return process_mapserv_request(workspace, query_parameters)

    def handle_get_map_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        return self._handle_layer_based_request(workspace, query_parameters)

    def handle_get_feature_info_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        return self._handle_layer_based_request(workspace, query_parameters)

    def _handle_layer_based_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
    ):
        mapserv_query_params = query_parameters.copy()
        layers = [i for i in query_parameters.get("layers").split(",")]
        for layer in workspace.layers.filter(name__in=layers):
            layer_params = get_layer_params(layer, mapserv_query_params)
            mapserv_query_params.update(layer_params)
        return process_mapserv_request(workspace, mapserv_query_params)


class CustomWmsHandler(BaseWmsVersion130Handler):

    def handle_get_feature_info_request(
            self,
            workspace: models.Workspace,
            query_parameters: typing.Dict,
            request: HttpRequest
    ):
        return HttpResponse("Yo!")


def get_requested_dimensions(
        layer: models.Layer,
        query_parameters: typing.Dict[str, str]
):
    requested_dimensions = []
    for dimension in layer.spatial_dataset.dimensions.filter(is_public=True):
        deserialized = deserialize_dimension(dimension, query_parameters)
        requested_dimensions.append(deserialized)
    return requested_dimensions


def deserialize_dimension(
        dimension: models.Dimension,
        query_parameters: typing.Dict[str, str]
) -> RequestedDimension:
    name = dimension.name
    standard_names = ("time", "elevation")
    requested_name = name if name in standard_names else f"dim_{name}"
    requested_value = query_parameters.get(requested_name, dimension.default)
    parsed_values = parse_dimension_value(dimension.type, requested_value)
    return RequestedDimension(
        dimension=dimension,
        requested_name=requested_name,
        raw_value=requested_value,
        parsed_values=parsed_values
    )


def serialize_dimension(requested_dimension: RequestedDimension) -> str:
    """Serialize a dimension request to send it to MapServer."""
    result = ""
    dim_name = requested_dimension.dimension.name
    # NOTE:
    # - `time` dimensions are serialized with `%Y-%m-%d %H:%M:%S` (the default
    #   when you call str() on a datetime)
    # - other temporal dimensions are serialized with `%Y-%m-%dT%H:%M:%S`
    for range_start, range_end in requested_dimension.parsed_values:
        if isinstance(range_start, dt.datetime) and dim_name != "time":
            temporal_format = "%Y-%m-%dT%H:%M:%S"
            start = range_start.strftime(temporal_format)
            end = (
                range_end.strftime(temporal_format) if range_end is not None
                else None
            )
        else:
            start = str(range_start)
            end = str(range_end) if range_end is not None else None
        result += str(start)
        if end is not None:
            result += f"/{end}"
        result += ","
    else:
        result = result[:-1]  # remove the last ','
    return result


def parse_dimension_value(
        dimension_type: models.Dimension.DimensionType,
        raw_value: str
) -> typing.List[
    typing.Tuple[
        typing.Union[str, float, dt.datetime],
        typing.Optional[typing.Union[str, float, dt.datetime]],
    ]
]:
    parsed_values = []
    for value in raw_value.split(","):
        start, end = value.partition("/")[::2]
        end = end if end != "" else None
        if dimension_type == models.Dimension.DimensionType.TEMPORAL:
            parsed_values.append((
                dateutil.parser.parse(start),
                dateutil.parser.parse(end) if end is not None else None,
            ))
        elif dimension_type == models.Dimension.DimensionType.NUMERICAL:
            parsed_values.append((
                float(start),
                float(end) if end is not None else None
            ))
        elif dimension_type == models.Dimension.DimensionType.CATEGORICAL:
            parsed_values.append((
                str(start),
                end
            ))
        else:
            raise ValueError(f"Cannot parse dimension_type: {dimension_type}")
    return parsed_values


def find_granule_band(
        requested_dimensions: typing.List[RequestedDimension]
) -> typing.Optional[int]:
    dim_parts = []
    for requested_dim in requested_dimensions:
        where_fragment = get_fragment(requested_dim.dimension)
        dim_parts.append((where_fragment, requested_dim.parsed_values[0][0]))
    logger.debug(f"dim_parts: {dim_parts}")
    where_clause = " AND ".join(part[0] for part in dim_parts)
    logger.debug(f"where_clause: {where_clause}")
    logger.debug(f"cursor args: {[part[1] for part in dim_parts]}")
    with connection.cursor() as cursor:
        cursor.execute(
            f"SELECT storage_details->>'band' AS band FROM geospaca_granule "
            f"WHERE {where_clause}",
            [part[1] for part in dim_parts]
        )
        row = cursor.fetchone()
    return row[0] if row is not None else None


def get_fragment(dimension: models.Dimension):
    if dimension.type == models.Dimension.DimensionType.TEMPORAL:
        result = (
            f"to_timestamp(dimension_values->>'{dimension.name}', "
            f"'YYYY-MM-DD HH24:MI:SS') = %s"
        )
    elif dimension.type == models.Dimension.DimensionType.NUMERICAL:
        result = f"cast(dimension_values->>'{dimension.name}' AS float) = %s"
    else:
        result = f"dimension_values->>'{dimension.name}' = %s"
    return result


def get_layer_params(
        layer: models.Layer,
        query_parameters: typing.Dict
) -> typing.Dict:
    requested_dimensions = get_requested_dimensions(
        layer, query_parameters)
    logger.debug(f"requested_dimensions: {requested_dimensions}")
    layer_params = {}
    for requested_dim in requested_dimensions:
        name = requested_dim.requested_name
        layer_params[name] = serialize_dimension(requested_dim)
    granule_band_index = find_granule_band(requested_dimensions)
    band_name = mapfilehandler.get_layer_band_name(layer.spatial_dataset)
    logger.debug(f"granule_band_index: {granule_band_index}")
    layer_params.update({
        band_name: granule_band_index,
    })
    return layer_params


def process_mapserv_request(
        workspace: models.Workspace,
        query_parameters: typing.Dict
) -> HttpResponse:
    mapserv_query_params = query_parameters.copy()
    mapserv_query_params.update({
        "map": mapfilehandler.get_mapfile_path(workspace.name)
    })
    logger.debug(f"mapserv_query_params: {mapserv_query_params}")
    mapserv_response = mapservhandler.call_mapserv(mapserv_query_params)
    response_headers, body, errors = mapserv_response
    logger.debug(f"mapserver stderr: {errors}")
    response_content_type = response_headers.get("Content-Type")
    logger.debug(f"response_headers: {response_headers}")
    logger.debug(f"response_content_type: {response_content_type}")
    return HttpResponse(
        body,
        content_type=response_headers.get("Content-Type")
    )
