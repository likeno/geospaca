"""django_geospaca URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.gis import admin
from django.views.generic import TemplateView
from django.urls import (
    include,
    path
)
from rest_framework.schemas import get_schema_view

import geospaca.views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("geospaca/", include("geospaca.urls")),
    path(
        "ogc/<workspace_name>/",
        geospaca.views.OgcLegacyView.as_view(),
        name="ogc"
    ),
    path(
        "ogc/wms/<workspace_name>/",
        geospaca.views.OgcLegacyWmsView.as_view(),
        name="wms"
    ),
    path("api/", include("geospaca.api.urls")),
    path(
        "api-auth/",
        include("rest_framework.urls", namespace="rest_framework")
    ),
    path(
        "openapi",
        get_schema_view(
            title="geospaca spatial server",
            description="A spatial server for your data",
            version="0.0.1",
            urlconf="geospaca.api.urls",
            url="/api/",
        ),
        name="openapi-schema"
    ),
    path(
        "openapi-ui",
        TemplateView.as_view(
            template_name="geospaca/swagger-ui-dist-index.html",
        )
    )
]
