# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on 
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to 
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [0.7.1] - 2020-03-28

### Fixed

-  Fix incorrect regular expression used for the mapserver VALIDATION of 
   temporal dimensions
   

## [0.7.0] - 2020-03-22

### Added
-  Use raster files that make use of GDAL subdatasets - This is 
   made possible by allowing the new `subdataset_prefix` and 
   `subdataset_suffix` fields on a granule's `storage_details`
-  Non public dimensions. These can be used internally, for example 
   in filesystem-based storage configuration. They are not publicized by WMS


## [0.6.0] - 2020-03-11

### Added

-  basic auth to API calls that modify data
-  Initial filtering for a coverage's granules based on dimension values

### Changed

-  Reimplement geospaca based on django, django-rest-framework and 
   django-rest-framework-json-api


## [0.4.0] - 2020-02-10

### Added

-  API endpoint for updating a coverage now allows refreshing the underlying 
   tileindex layer
   
### Changed

-  Creating a new granule no longer performs an automatic refresh of the 
   underlying tileindex layer

### Fixed

-  Development now uses `uvicorn` directly in order to provide proper hot 
   reloading of changed files


## [0.3.2] - 2020-02-06

### Fixed

-  Incorrect granule-related database queries


## [0.3.1] - 2020-02-05

### Fixed

-  Incorrect coverage-related database queries


## [0.3.0] - 2020-02-04

### Added

-  Add support for layers of type `vector_field`
-  Add support for mapserver `FONTSET`

### Fixed

-  Broader validation rules for temporal dimensions
   
   
## [0.2.1] - 2020-01-21

### Fixed

-  Incorrect coverage-related database queries

   
## [0.2.0] - 2020-01-15

### Added

-  Added `list workspaces` CLI command 
-  Implemented different layer types
-  Add isoline layer type for rendering isolines
-  Added settings to control database pool size, defaulting to an initial size
   of zero
-  Added settings to control dramatiq worker threads , defaulting to a 
   single thread per dramatiq worker one
   
### Changed

-  CLI uses internal server URL
-  CLI command `create workspace` now checks if workspace with the 
   provided name already exists and returns it instead of trying to 
   create it again (which would be an error, since workspace names are unique)
-  CLI command `load-data` is now able to use pre-existing response_handlers
   

## [0.1.0] - 2019-11-20

### Added

-  Initial support for OGC WMS requests
-  Basic web API endpoints for managing workspaces, layers, coverages and 
   related resources
-  Automatic mapfile generation based on the contents of the DB
-  Automatic tileindex generation
-  A CLI to perform most resource creation tasks - this uses the web API 
   underneath
-  CI configuration that runs multiple tests and reports on code coverage
   
   
[unreleased]: https://gitlab.com/likeno/geospaca/compare/v0.7.1...master
[0.7.1]: https://gitlab.com/likeno/geospaca/-/tags/v0.7.1
[0.7.0]: https://gitlab.com/likeno/geospaca/-/tags/v0.7.0
[0.6.0]: https://gitlab.com/likeno/geospaca/-/tags/v0.6.0
[0.4.0]: https://gitlab.com/likeno/geospaca/-/tags/v0.4.0
[0.3.2]: https://gitlab.com/likeno/geospaca/-/tags/v0.3.2
[0.3.1]: https://gitlab.com/likeno/geospaca/-/tags/v0.3.1
[0.3.0]: https://gitlab.com/likeno/geospaca/-/tags/v0.3.0
[0.2.1]: https://gitlab.com/likeno/geospaca/-/tags/v0.2.1
[0.2.0]: https://gitlab.com/likeno/geospaca/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/likeno/geospaca/-/tags/v0.1.0
