geospaca
========

An API-first server for your geospatial data


Testing
-------

Tests are implemented with pytest. They can be ran by calling::

    python3 -m pytest --cov --exitfirst --verbose --capture=no

Custom pytest markers that can be used to selectively run tests. Some examples::

      python3 -m pytest --cov -x -v -s -m integration  # run only integration tests
      python3 -m pytest --cov -x -v -s -m workspaces  # run only tests for workspace-related functionality
      python3 -m pytest --cov -x -v -s -m update  # run only tests that perform update of resources
      python3 -m pytest --cov -x -v -s -m "update and styles"  # run tests that perform update of resources of type `styles`
      python3 -m pytest --cov -x -v -s -m "functional and retrieve and workspaces"  # run functional tests that perform retrieval of `workspaces`


Development
-----------

Docker swarm development environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Setup a local docker swarm - check https://dockerswarm.rocks/ for how to
  do this if needed

* Either build a docker image for the project with::

      docker build -t registry.gitlab.com/likeno/geospaca -f docker/Dockerfile .

  Or pull from the repo's docker registry with::

      docker login registry.gitlab.com
      docker pull registry.gitlab.com/likeno/geospaca


* Generate the following files:

  * ``docker/secrets/database-password``
  * ``docker/secrets/web-database-url``
  * ``docker/secrets/web-message-queue-rabbitmq-url``

* Then launch the development stack with::

      export STACK=geospaca-dev
      docker stack deploy --compose-file docker/docker-cloud-dev.yml --prune $STACK


Geospaca is now available at http://localhost:8000. The repo's root has been
bind-mounted into the ``geospaca-web`` and ``geospaca-worker`` containers,
which means you may modify the code and chages are recognized inside them.
Both ``uvicorn`` and ``dramatiq`` are launched with the respective
``--reload`` flags too, so they will update whenever a change is made.

When done, you may bring the stack down with::

    docker stack rm $STACK


Management commands
-------------------

The ``geospaca-admin`` cli tool has some management commands that are
useful for development.

It can be used by calling::

    geospaca-admin <command> [<command args>]



Changing database schema
------------------------

We use ``alembic`` to manage DB schema, so, whenever needed to alter the
database, you'll need to:

* Modify the DB model definitions in ``geospaca/db/models.py``

* Ask alembic to generate a migration file::

      geospaca-admin make-migrations <some-message>

* Inspect the migration file and verify its correctness

* Then apply the migrations::

      geospaca-admin migrate

If you want to wipe the migrations and start fresh, then:

* ``git rm`` any previous migration file(s)

* drop the database

* run ``geospaca-admin make-migrations initial-setup`` - This will create the
  new migrations file

* run ``geospaca-admin migrate`` - This will generate the database tables


Building the docker image used for gitlab-ci
--------------------------------------------

We use a custom docker base image for gitlab-ci. The relevant ``Dockerfile``
can be built and pushed with the following::

    docker login registry.gitlab.com
    docker build -t registry.gitlab.com/likeno/geospaca/gitlab-ci:py3.7-poetry0.12.0 -f docker/gitlab-ci.Dockerfile .
    docker push registry.gitlab.com/likeno/geospaca/gitlab-ci:py3.7-poetry0.12.0


